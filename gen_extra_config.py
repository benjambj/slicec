#!/usr/bin/env python

from __future__ import print_function

import re
import sys

def main():
    config = dict()
    dim_size_regex = re.compile("n(\\d+)")
    r_values = []
    n_dims = 0
    for line in sys.stdin:
        print(line)
        fields = line.split()
        try:
            config[fields[1]] = int(fields[2])
            dim_size = dim_size_regex.match(fields[1])
            if dim_size:
                n_dims = max(n_dims, int(dim_size.group(1)))
        except ValueError:
            pass

    i_strs = [str(i) for i in range(1, n_dims+1)]
    print("#define colon_comma_list", ", ".join([":" for _ in i_strs]))
    print("#define n_comma_list", ", ".join(["n" + i for i in i_strs]))
    print("#define m_size_param_list",
          ", ".join(["m_size_" + i for i in i_strs]))
    for i in i_strs:
        print("#define", "r" + i,(config["b"+i]-config["a"+i])//config["s"+i]+1)
    print("#define r_comma_list", ", ".join(["r" + i for i in i_strs]))
    print("#define index_expr_list", ", ".join(
        ":".join(map(str,[config["a"+i], config["b"+i], config["s"+i]]))
        for i in i_strs
    ))

if __name__ == '__main__':
   main()
