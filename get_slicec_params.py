#!/usr/bin/env python
# encoding: utf-8

from __future__ import print_function

import re
import sys

def macro_def_pattern(name_pattern, value_pattern):
    return re.compile("^\\#define ({}) ({})$".format(name_pattern, value_pattern))

def main():
    dims = []
    dim_arg = ''
    dim_sep = ''
    expr_arg = 'M('
    unroll_arg = ''

    dim_pattern = re.compile('^\\#define n\\d+ (\\d+)$')
    expr_pattern = re.compile('^\\#define index_expr_list (.*)$')
    unroll_pattern = macro_def_pattern("unroll_config", ".*")
    L1_prefdim_pattern = macro_def_pattern("L1_prefetch_dim", "\\d+")
    L1_prefdist_pattern = macro_def_pattern("L1_prefetch_dist", "\\d+")
    L2_prefdim_pattern = macro_def_pattern("L2_prefetch_dim", "\\d+")
    L2_prefdist_pattern = macro_def_pattern("L2_prefetch_dist", "\\d+")
    use_gather_pattern = macro_def_pattern("use_gather", ".*")

    n_patterns = 8

    args = [""] * n_patterns
    args[1] = "M("

    def try_match_arg(line, pattern, num, updated_arg=lambda prev, match: match.group(2)):
        match = pattern.match(line)
        if match:
            args[num] = updated_arg(args[num], match)

    with open(sys.argv[1]) as cfg_file:
        for line in cfg_file:
            try_match_arg(line, dim_pattern, 0, lambda prev, match: match.group(1) if len(prev)==0 else prev + "x" + match.group(1))
            try_match_arg(line, expr_pattern, 1, lambda prev, match: prev + match.group(1))
            try_match_arg(line, unroll_pattern, 2)
            try_match_arg(line, L1_prefdim_pattern, 3)
            try_match_arg(line, L1_prefdist_pattern, 4)
            try_match_arg(line, L2_prefdim_pattern, 5)
            try_match_arg(line, L2_prefdist_pattern, 6)
            try_match_arg(line, use_gather_pattern, 7)

    args[1] += ")"

    print(' '.join(['"{}"'.format(arg) for arg in args]))

if __name__ == '__main__':
   main()
