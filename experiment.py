#!/usr/bin/env python
# encoding: utf-8
from __future__ import print_function

import argparse
import hashlib
import io
import os
import re
import subprocess
import sys

class ExperimentResult(object):
    def __init__(self, results=None, error=None):
        if (results is None) == (error is None):
            raise Exception(
                "Program bug: Should supply either results or error.")
        self.results = results
        self.error = error
    def is_error(self):
        return not self.error is None

class Experiment(object):
    # Virtual functions:
    # - get_program_arguments(self, point):
    #    Returnerer en liste med argument til eksperimentbinærfila for
    #    et gitt punkt i eksperimentrommet.
    def get_program_arguments(self, point):
        raise NotImplementedError("get_program_arguments() must be " +
                                  "overridden in subclasses of Experiment")
    #
    # - get_result_fields(self): returnerer en liste strenger
    #    Returnerer en liste med strenger, som er navnene på resultatene
    #    fra eksperimentet.
    def get_result_fields(self):
        raise NotImplementedError("get_result_fields() must be " +
                                  "overridden in subclasses of Experiment")
    #
    # - get_results(self, outstream):
    #    Returnerer en ExperimentResult, som har feltene:
    #       - def is_error(), returnerer True hvis error.
    #       - results, som er en dict med resultatdata fra
    #         eksperimentet. Nøklene er de samme som get_result_fields()
    #       - error, en streng som beskriver feilen.
    def get_results(self):
        raise NotImplementedError("get_results() must be " +
                                  "overridden in subclasses of Experiment")
    #
    # - construct_compile_time_args_parser(self):
    #     Konstruerer en argparse.ArgumentParser
    def construct_compile_time_args_parser(self):
        raise NotImplementedError("construct_compile_time_args_parser() must "+
                                  "be overridden in subclasses of Experiment")
    #
    # - construct_runtime_args_parser(self, parent):
    #     Konstruerer en argparse.ArgumentParser som arver fra parent.
    def construct_runtime_args_parser(self, parent):
        raise NotImplementedError("construct_runtime_args_parser() must "+
                                  "be overridden in subclasses of Experiment")
    #
    # - get_experiment_space(self):
    #     En generator som returnerer alle (navngitte) punkt
    #     i iterasjonsrommet
    def get_experiment_space(self):
        raise NotImplementedError("get_experiment_space() must "+
                                  "be overridden in subclasses of Experiment")
    #
    # - get_dimension_names(self):
    #     Returnerer en liste av navn lik nøklene til
    #     get_experiment_dimensions()
    def get_dimension_names(self):
        raise NotImplementedError("get_dimension_names() must "+
                                  "be overridden in subclasses of Experiment")

    def __init__(self, name):
        self.name = name
        ct_arg_parser = self.construct_compile_time_args_parser()
        rt_arg_parser = self.construct_runtime_args_parser(
            parent=self.construct_base_runtime_args_parser())
        ct_opts, rem_args = ct_arg_parser.parse_known_args()
        self.ct_cfg = CompilationConfig(ct_opts)
        rt_opts = rt_arg_parser.parse_args(rem_args)
        self.rt_cfg = RuntimeConfig(rt_opts)
        self.config_filename = "configs/{}".format(self.ct_cfg.config_id())
        self.bin_filename = "bin/{}_{}".format(self.name, self.ct_cfg.config_id())
        self.result_filename = "results/tab_{}".format(self.ct_cfg.config_id())

    def construct_base_runtime_args_parser(self):
        parser = argparse.ArgumentParser(
            description='Run experiment ' + self.name, add_help=False)
        parser.add_argument('--threads', nargs='+', default=[228],
                            type=int,
                            help="A list of thread counts to use.")
        parser.add_argument('--runs', type=int, default=1,
                            help="The number of repetitions.")
        parser.add_argument('--affinities', default=["scatter"], nargs='+',
                            help="The thread mapping affinity setting.")
        org_group = parser.add_argument_group("Org file output")
        org_output_group = org_group.add_mutually_exclusive_group(required=True)
        org_output_group.add_argument('-o', '--org-outfile', dest="org_filename",
                                    help="Dump results to selected org file in addition to stdout")
        org_output_group.add_argument('--no-org-outfile', dest="use_orgfile", action="store_false", default=True,
                                    help="Only print results to stdout.")
        org_append_group = org_group.add_mutually_exclusive_group(required=True)
        org_append_group.add_argument('--org-append', default=False, action="store_true", dest="org_append",
                                    help="If the file selected with --org-outfile exists, " +
                                    "append to it instead of creating a new file.")
        org_append_group.add_argument('--org-new-file-on-conflict', action="store_true", dest="org_new_file",
                                            help="If the file selected with --org-outfile exists, " +
                                            "use <filename>-2.org instead.")
        result_group = parser.add_mutually_exclusive_group(required=True)
        result_group.add_argument('--overwrite-existing',
                                help="If result file exists, overwrite its contents.",
                                default=False, dest="overwrite_result", action="store_true")
        result_group.add_argument('--keep-existing',
                                help="If result file exists and is newer than the binary, do not run the experiment.",
                                default=False, dest="overwrite_result", action="store_false")
        return parser

    def run(self):
        if not os.path.exists(self.config_filename):
            print("Writing configuration to file {}".format(self.config_filename))
            if not os.path.exists(os.path.dirname(self.config_filename)):
                os.mkdir(os.path.dirname(self.config_filename))
            with open(self.config_filename, 'w') as config_file:
                config_file.write(self.ct_cfg.macro_definitions())

        exit_code = subprocess.call(["make", self.bin_filename])
        if exit_code == 0:
            if os.path.exists(self.result_filename):
                mod_time = os.path.getmtime(self.result_filename)
                if mod_time >= os.path.getmtime(self.bin_filename):
                    if self.rt_cfg.overwrite_result:
                        print("Result file {} exists, discarding old results.".
                            format(self.result_filename))
                    else:
                        print("Result file {} is already up-to-date!".
                            format(self.result_filename))
                        return 0
        if exit_code == 0:
            cmd = ["scp", self.bin_filename, "mic0:"]
            print(' '.join(cmd))
            exit_code = subprocess.call(cmd)
        if exit_code == 0:
            with ResultLog(self.ct_cfg, self.rt_cfg, self) as log:
                self.run_experiments(log)
        return exit_code

    def run_experiments(self, log):
        log.print_header()
        for i in range(1, self.rt_cfg.runs + 1):
            for a in self.rt_cfg.affinities:
                for point in self.get_experiment_space():
                # for m, n, l in rt_cfg.get_dimensions():
                    for t in self.rt_cfg.threads:
                        self.run_experiment(i, a, point, t, log)

    def run_experiment(self, i, a, point, t, log):
        prog_args = ' '.join(self.get_program_arguments(point))
        p1 = subprocess.Popen(
            ['ssh',
             'mic0',
             '. .bashrc; ' +
             'OMP_NUM_THREADS={t} KMP_AFFINITY={aff} ./{config} {args}'.
             format(t=t, aff=a,
                    config=os.path.basename(self.bin_filename),
                    args=prog_args)
            ], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = p1.communicate()
        if len(err) != 0:
            print(("Error with run #{}, affinity={}, " +
                   "nthreads = {}, args = {}: {}").
                  format(i, a, t, prog_args, err, file=sys.stderr))
            sys.stderr.flush()
        else:
            outstream = io.TextIOWrapper(io.BufferedReader(io.BytesIO(out)))
            results = self.get_results(outstream)
            if results.is_error():
                self.report_error(i, a, t, prog_args, results.error)
            else:
                log.log_results(i, a, t, point, results.results)

    def report_error(self, i, a, t, args, error_msg):
        print(("Incorrect results for run #{}, affinity={}, " +
               "nthreads = {}, args = {}: {}").
                  format(i, a, t, args, error_msg, file=sys.stderr))
            # mkl_stats = tabulate.read_next_stats(outstream)
            # manual_stats = tabulate.read_next_stats(outstream)
            # if not error_reported(outstream, i, m, n, l, t):
            #     log.log_results(i, m, n, l, t, mkl_stats, manual_stats)

class CompilationConfig(object):
    def __init__(self, config_args):
        self.config_args = config_args

    def __str__(self):
        return self.macro_definitions()

    def macro_definitions(self):
        return ''.join(["#define " + str(k) + ' ' + str(e) + "\n"
                        for k, e in sorted(vars(self.config_args).items())
                        if not e is None])

    def config_id(self):
        return hashlib.md5(self.macro_definitions().encode()).hexdigest()

class RuntimeConfig(object):
    def __init__(self, config_args):
        self.__dict__ = dict(vars(config_args))
        # self.dims = get_dimensions(config_args)
        # self = config_args
        # self.thread_str = ' '.join(str(x) for x in self.threads)
        # self.M_str = ' '.join([str(x) for x in self.M])
        # if self.N:
        #     self.N_str = ' '.join([str(x) for x in self.N])
        # if self.L:
        #     self.L_str = ' '.join([str(x) for x in self.L])

    # def __str__(self):
    #     return self.variable_settings_for_make()

    # def get_dimensions(self):
    #     if self.use_M_as_N:
    #         if self.use_M_as_L or self.use_N_as_L:
    #             dims = [(m, m, m) for m in self.M]
    #         else:
    #             dims = [(m, m, l) for m in self.M for l in self.L]
    #     else:
    #         if self.use_M_as_L:
    #             dims = [(m, n, m) for m in self.M for n in self.N]
    #         elif self.use_N_as_L:
    #             dims = [(m, n, n) for m in self.M for n in self.N]
    #         else:
    #             dims = [(m, n, l) for m in self.M for n in self.N for l in self.L]
    #     return dims

    def need_org_file_header(self):
        append_to_existing = self.org_append and os.path.exists(self.org_filename)
        return self.use_orgfile and not append_to_existing

    def open_org_file(self):
        if not self.use_orgfile:
            return RuntimeConfig.dummy_file()
        org_perm = 'a' if self.org_append else 'w'
        filename = self.org_filename
        if not self.org_append:
            filename = RuntimeConfig.find_unique_version(filename)
        return open(filename, org_perm)

    @staticmethod
    def dummy_file():
        class DummyFile:
            def __enter__(self):
                pass
            def __exit__(self, tpe, value, tb):
                pass
        return DummyFile()

    @staticmethod
    def find_unique_version(filename):
        orig_filename = filename
        base, ext = os.path.splitext(filename)
        counter = 1
        while os.path.exists(filename):
            counter += 1
            filename = base + '-' + str(counter) + ext
        if os.path.exists(orig_filename):
            print(("Warning: file {} exists and --org-append not specified. " +
                    "Using file name {} instead.").
                  format(orig_filename, filename), file=sys.stderr)
        return filename

    # def variable_settings_for_make(self):
    #     v = 'RUNS={runs} THREADS="{thread_str}"'
    #     if self.use_M_as_L and self.use_M_as_N:
    #         v += ' SQUARE_SIZES="{M_str}"'
    #     elif self.use_M_as_L:
    #         v += ' SIZES_M="{M_str}" SIZES_N="{N_str}"'
    #     elif self.use_N_as_L:
    #         print('Error: option use_N_as_L is not possible to pass through the Makefile yet', file=sys.stderr)
    #         sys.exit(1)
    #     else:
    #         v += ' SIZES_M="{M_str}" SIZES_N="{N_str}" SIZES_L="{L_str}"'
    #     if self.keep_mkl:
    #         v += ' KEEP_MKL=y'
    #     if self.use_orgfile:
    #         v += ' ORGF="{org_filename}"'
    #     if self.org_append:
    #         v += ' ORG_APPEND=y'
    #     return v.format(**vars(self))

class ResultLog(object):
    def __init__(self, ct_config, runtime_config, experiment):
        # self.keep_mkl = runtime_config.keep_mkl
        self.use_orgfile = runtime_config.use_orgfile
        self.need_org_header = runtime_config.need_org_file_header()
        self.orgfile = runtime_config.open_org_file()
        self.parameters = ResultLog.get_parameters(ct_config)
        self.dimension_names = experiment.get_dimension_names()
        self.result_fields = experiment.get_result_fields()
        self.result_file = open(experiment.result_filename, 'w')
        self.header, self.sep_line, self.col_just = \
            self.create_header()
        self.result_fmt = '|' + '|'.join(
            ['{:'+str(j)+'}' for j in self.col_just]) + '|'

    def __enter__(self):
        self.result_file.__enter__()
        self.orgfile.__enter__()
        return self

    def __exit__(self, tpe, value, tb):
        try:
            self.orgfile.__exit__(tpe, value, tb)
        finally:
            self.result_file.__exit__(tpe, value, tb)

    @staticmethod
    def get_parameters(ct_config):
        params = vars(ct_config.config_args)
        return dict([(k, params[k]) if params[k] is not None else (k, "None") for k in params])

    def create_header(self):
        # header = "| run |   m   |   n   |   l   | "
        header = "| run | affinity | threads |"
        # rt_dims = experiment.get_
        # statnames = ["fpc ", "gflops ", "gbps ", "seconds "]
        # if self.keep_mkl:
        #     header += '|'.join([" mkl_"+n for n in statnames]) + '|'
        #     statnames = [" manual_" + n for n in statnames]
        # else:
        #     statnames = ["   " + n + "  " for n in statnames]

        for dim in sorted(self.dimension_names):
            header += dim + '|'
        for resf in sorted(self.result_fields):
            header += resf + '|'

        for pname in sorted(self.parameters):
            col_len = max(len(pname),
                          len(str(self.parameters[pname]))) + 2
            title_pad = max(col_len - len(pname), 0)
            header += pname.rjust(
                len(pname) + title_pad//2).ljust(col_len) + '|'
        # header += " threads |"
        sep_line = '|'
        coljust = []
        curjust = 0
        for l in header[1:-1]:
            if l == '|':
                sep_line += '+'
                coljust.append(curjust)
                curjust = 0
            else:
                sep_line += '-'
                curjust += 1
        if curjust != 0:
            coljust.append(curjust)
        sep_line += '|'
        return header, sep_line, coljust

    def print_str(self, string, to_stdout=True, to_result=True,
                  to_org=True):
        if to_stdout:
            print(string)
            sys.stdout.flush()
        if to_result:
            print(string, file=self.result_file)
        if to_org and self.use_orgfile:
            print(string, file=self.orgfile)

    def print_header(self):
        self.print_str(self.header + "\n" + self.sep_line,
                       to_org=self.need_org_header)

    def log_results(self, r, a, t, point, results):
        fmt_args = [r, a, t]
        fmt_args.extend([point[k] for k in sorted(point)])
        fmt_args.extend([results[k] for k in sorted(results)])
        fmt_args.extend([self.parameters[k] for k in sorted(self.parameters)])
        result_str = self.result_fmt.format(*fmt_args)
        self.print_str(result_str)

# def main():
#     ct_parser = construct_compile_flag_parser()
#     rt_parser = construct_runtime_arg_parser()
#     ct_opts, rt_args = ct_parser.parse_known_args()
#     ct_cfg = CompilationConfig(ct_opts)
#     rt_opts = rt_parser.parse_args(rt_args)
#     rt_cfg = RuntimeConfig(rt_opts)

#     config_filename = "configs/{}".format(ct_cfg.config_id())
#     bin_filename = "bin/mic/symm_{}".format(ct_cfg.config_id())
#     result_filename = "results/mic/tab_{}".format(ct_cfg.config_id())

#     if not os.path.exists(config_filename):
#         print("Writing configuration to file {}".format(config_filename))
#         with open(config_filename, 'w') as config_file:
#             config_file.write(ct_cfg.macro_definitions())

#     exit_code = subprocess.call(["make", bin_filename])
#     if exit_code == 0:
#         if os.path.exists(result_filename):
#             mod_time = os.path.getmtime(result_filename)
#             if mod_time >= os.path.getmtime(bin_filename):
#                 if rt_cfg.overwrite_result:
#                     print("Result file {} exists, discarding old results.".
#                           format(result_filename))
#                 else:
#                     print("Result file {} is already up-to-date!".
#                           format(result_filename))
#                     return
#     if exit_code == 0:
#         cmd = ["scp", bin_filename, "mic0:"]
#         print(' '.join(cmd))
#         exit_code = subprocess.call(cmd)
#     if exit_code == 0:
#         with ResultLog(ct_cfg, rt_cfg, result_filename) as log:
#             run_experiments(rt_cfg, bin_filename, log)
#     sys.exit(exit_code)

# def run_experiments(runtime_config, bin_filename, log):
#     log.print_header()
#     for i in range(1, runtime_config.runs + 1):
#         for m, n, l in runtime_config.get_dimensions():
#             for t in runtime_config.threads:
#                 p1 = subprocess.Popen(
#                     ['ssh',
#                     'mic0',
#                     '. .bashrc; OMP_NUM_THREADS={t} ./{config} {m} {n} {l}'.format(
#                         t=t, config=os.path.basename(bin_filename), m=m, n=n, l=l)
#                     ], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
#                 out, err = p1.communicate()
#                 if len(err) != 0:
#                     print("Error with run #{}, dims (m, n, l) = ({}, {}, {}), nthreads = {}: {}".format(
#                         i, m, n, l, t, err), file=sys.stderr)
#                     sys.stderr.flush()
#                 else:
#                     outstream = io.TextIOWrapper(io.BufferedReader(io.BytesIO(out)))
#                     mkl_stats = tabulate.read_next_stats(outstream)
#                     manual_stats = tabulate.read_next_stats(outstream)
#                     if not error_reported(outstream, i, m, n, l, t):
#                         log.log_results(i, m, n, l, t, mkl_stats, manual_stats)

# def error_reported(outstream, i, m, n, l, t):
#     line = outstream.readline()
#     while line:
#         if re.match(r"Error at (.*)", line):
#             print("Incorrect results for run #{}, dims (m, n, l) = ({}, {}, {}), nthreads = {}: {} ({})".
#                   format(i, m, n, l, t, line.strip(), outstream.readline().strip()), file=sys.stderr)
#             return True
#         line = outstream.readline()
#     return False

# if __name__ == '__main__':
#    main()
