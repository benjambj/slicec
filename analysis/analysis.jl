using GLM
using HypothesisTests
using StatsBase

function print_dist_cmp(x, y, x_name="X", y_name="Y")
    println(x_name, ": ", mean(x), " ± ", std(x))
    println(y_name, ": ", mean(y), " ± ", std(y))
    println(UnequalVarianceTTest(x, y))
end
