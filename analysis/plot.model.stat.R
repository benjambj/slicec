source("create.grouped.models.R")

library(dplyr)
library(ggplot2)

plot.model.stat <- function(data, group.vars=character(0), model.formula=y~x) {
  models <- create.grouped.models(data, model.formula, group.vars)
  ggplot(models, aes(x=x, group="")) +
    labs(x=sprintf("%s", paste(group.vars, sep=", ", collapse=", ")))
}
