library(dplyr)
library(reshape2)
library(stringr)

melt.seconds <- function(data) {
  obsv.names <- names(data)
  runtime.obsv <- obsv.names[str_detect(obsv.names, "^seconds\\d+")]
  data <- melt(
    data,
    measure.vars = runtime.obsv,
    value.name="seconds",
    variable.name="repetition")
  data <- mutate(
             data,
             repetition = as.numeric(repetition))
  data
}
