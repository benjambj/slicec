library(stringr)

add.mean.runtime <- function(data) {
  fields <- names(data)
  runtime.fields <- fields[str_detect(fields, fixed("seconds"))]
  data$mean.seconds = rowMeans(data[,runtime.fields])
  data
}
