using DataFrames
import DataFrames.getindex
import CSV
using DataStreams

type WideResult
    experimentname
    df
end

type LongResult
    experimentname
    df
end

getfields(df, regex) = [s for s in names(df) if ismatch(regex, string(s))]
getindex(df::DataFrame, regex::Regex) = df[getfields(df, regex)]

getsrcdims(df) = df[r"^n\d+$"]
getresdims(df) = df[r"^r\d+$"]

function addresultdims!(df)
    ndims = maximum(df[:ndims])
    for d in 1:ndims
        df[Symbol(string("r", d))] = 0
        for i in 1:size(df, 1)
            df[i, Symbol(string("r", d))] =
                (df[i, Symbol(string("b", d))] - df[i, Symbol(string("a", d))]) /
                df[i, Symbol(string("s", d))]
        end
    end
end

function numdims(df)
    count = 0
    srcd = getsrcdims(df)
    for i in 1:size(srcd, 2)
        if !isna(srcd[1,i])
            count = count + 1
        end
    end
    count
end

resultsize(df) =
    vec(mapslices(prod, convert(Array, getresdims(df)), 2) .* df[:real_width])

function rowprod(df)
    res = zeros(size(df, 1))
    for i in 1:size(df, 1)
        res[i] = 1
        for j in 1:size(df, 2)
            res[i] *= df[i,j]
        end
    end
    res
end

function dimensionstring(dfdims, ndims)
    [join(convert(Array, dfdims[i,1:ndims]), "x") for i in 1:size(dfdims, 1)]
end

function readresult(::Type{WideResult}, expr::AbstractString)
    filename = string("results/", expr, ".org")
    dfheader = CSV.read(filename, delim='|', rows=1)
    df = readtable(filename, separator='|', skipstart=2, nastrings=["", "NA", "None"], makefactors=true)
    names!(df, map(n -> Symbol(strip(string(n))), names(dfheader)))
    df = df[:,2:end-1]

    df[:id] = 1:size(df, 1)
    ndims = numdims(df)
    df[:ndims] = ndims
    addresultdims!(df)
    df[:bytescopied] = rowprod(getresdims(df)) .* df[:real_width]
    df[:srcdim] = dimensionstring(getsrcdims(df), ndims)
    df[:resdim] = dimensionstring(getresdims(df), ndims)
    WideResult(expr, df)
end
readresult(::Type{LongResult}, expr::AbstractString) =
    LongResult(readresult(WideResult, expr).df)
readresult(expr::AbstractString) = readresult(WideResult, expr)

LongResult(wide::WideResult) =
    LongResult(wide.experimentname, stack(wide.df, [:ifort_s, :slicec_s]))
WideResult(long::LongResult) =
    WideResult(long.experimentname, unstack(long.df, :id, :variable, :value))

export WideResult,LongResult,readresult
