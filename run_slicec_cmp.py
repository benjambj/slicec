#!/usr/bin/env python
# encoding: utf-8

from __future__ import print_function

from experiment import *
from prefetch_stats import *

import argparse
import functools
import itertools
import sys


class MatrixSize(object):
    def __init__(self, size_str):
        self.n = list(map(int, size_str.split("x")))


class SlicingExperiment(Experiment):
    def __init__(self):
        Experiment.__init__(self, "slicec_cmp")
        self.experiment_space = []

    def get_program_arguments(self, point):
        return []

    def get_experiment_space(self):
        return [dict()]

    @staticmethod
    def size_of_type(elem_t):
        elem_ts = {"single": 4, "double": 8}
        assert elem_t in elem_ts, "Unknown element type" + elem_t
        return elem_ts[elem_t]

    def get_dimension_names(self):
        return []

    def get_result_fields(self):
        return [
            "validity", "      ifort_s      ", "      slicec_s      ",
            "     L1_V_i    ", "L1_O_i", "L1_I_i", "     L2_V_i     ",
            "L2_O_i", "L2_I_i"
        ]

    def get_results(self, outstream):
        return SlicingExperimentResult(outstream,
                                       self.compute_prefetch_quality_results())

    def compute_prefetch_quality_results(self):
        cfg = self.ct_cfg.config_args
        a = [cfg.a1, cfg.a2, cfg.a3, cfg.a4, cfg.a5, cfg.a6]
        b = [cfg.b1, cfg.b2, cfg.b3, cfg.b4, cfg.b5, cfg.b6]
        s = [cfg.s1, cfg.s2, cfg.s3, cfg.s4, cfg.s5, cfg.s6]
        r = [(b[i] - a[i]) // s[i] + 1 if b[i] else None
             for i in range(len(s))]
        VL = 64 // cfg.real_width
        r[0] = int((b[0] - a[0]) // max(VL, s[0])) + 1
        U = [int(u) for u in cfg.unroll_config.split("_")]
        L1Dist, L1Dim = cfg.L1_prefetch_dist, cfg.L1_prefetch_dim
        L2Dist, L2Dim = cfg.L2_prefetch_dist, cfg.L2_prefetch_dim
        T_L1Dim = time_per_iteration(L1Dim, r, U, cfg.Tc, cfg.To)
        T_L2Dim = time_per_iteration(L2Dim, r, U, cfg.Tc, cfg.To)

        results = dict()
        results["     L1_V_i    "] = fraction_valid_prefetches(
            num_unrolled_iterations(r[L1Dim], U[L1Dim]), L1Dist)
        results["L1_O_i"] = estimated_overshoot(L1Dist, T_L1Dim,
                                                cfg.M_expected)
        results["L1_I_i"] = num_unrolled_iterations(r[L1Dim], U[L1Dim])
        results["     L2_V_i     "] = fraction_valid_prefetches(
            num_unrolled_iterations(r[L2Dim], U[L2Dim]), L2Dist)
        results["L2_O_i"] = estimated_overshoot(L2Dist, T_L2Dim,
                                                cfg.M_expected)
        results["L2_I_i"] = num_unrolled_iterations(r[L2Dim], U[L2Dim])

        return results

    def construct_compile_time_args_parser(self):
        parser = argparse.ArgumentParser(
            description="Arguments controlling compilation of slice_test")
        parser.add_argument(
            "--real-width",
            dest="real_width",
            type=int,
            choices={4, 8},
            required=True,
            help="Matrix element size in bytes.")
        for i in range(1, 7):
            istr = str(i)
            parser.add_argument(
                "-n" + istr,
                type=int,
                default=None,
                help="The size of the matrix",
                dest="n" + istr)
            parser.add_argument(
                "-a" + istr,
                type=int,
                default=None,
                dest="a" + istr,
                help="Start indices")
            parser.add_argument(
                "-b" + istr,
                type=int,
                default=None,
                dest="b" + istr,
                help="End indices")
            parser.add_argument(
                "-s" + istr,
                type=int,
                default=None,
                dest="s" + istr,
                help="Index steps")

        parser.add_argument(
            "--repetitions",
            type=int,
            required=True,
            help="The number of repeated slicings")

        # parser.add_argument(
        #     "--unroll-depth",
        #     type=int,
        #     help="Unroll depth (num vectors copied in the unrolled loop)")

        parser.add_argument(
            "-M",
            "--expected-memory-latency",
            type=int,
            default=[300],
            dest="M_expected",
            help="Expected latency of a prefetch operation")

        parser.add_argument(
            "-Tc",
            "--cycles-per-vector-copy",
            type=int,
            default=[2],
            dest="Tc",
            help="Expected number of cycles per vector copy.")

        parser.add_argument(
            "-To",
            "--cycles-loop-overhead",
            type=int,
            default=[10],
            dest="To",
            help="Expected number of cycles of loop overhead.")

        parser.add_argument(
            "-Cmin",
            "--min-vector-copies",
            type=int,
            default=[1],
            dest="Cmin",
            help="The minimum number of vector copies in the unrolled loop body"
        )

        parser.add_argument(
            "-Cmax",
            "--max-vector-copies",
            type=int,
            default=[38],
            dest="Cmax",
            help="The maximum number of vector copies in the unrolled loop body"
        )

        parser.add_argument(
            "-Vmin",
            "--valid-prefetch-threshold",
            type=float,
            default=[0.95],
            dest="Vmin",
            help="Valid prefetch threshold")

        parser.add_argument(
            "-Omax",
            "--max-prefetch-overshoot",
            type=int,
            default=[1000],
            dest="Omax",
            help="The max number of cycles too early that prefetches should be")

        parser.add_argument("--L1-prefetch-dim", type=int)
        parser.add_argument("--L1-prefetch-dist", type=int)
        parser.add_argument("--L2-prefetch-dim", type=int)
        parser.add_argument("--L2-prefetch-dist", type=int)
        parser.add_argument("--unroll-config")

        parser.add_argument(
            "--use-gather",
            action="store_true")

        return parser

    # TODO: Add compile time arguments:
    # - M, T_c, T_o, C_min, C_max, V_min, O_max
    # These should be passed on from run_forts.py
    #
    # Then, add compile time arguments:
    # - L1PrefetchDim, L1PrefetchDist, L2PrefetchDim, L2PrefetchDist
    # - UnrollConfig (string, U1_U2_U3_...)
    # These should be calculated from run_forts.py, and passed to slicec
    # through the get_slicec_params.py script.
    #
    # Then, add results:
    # - L1_V_i, L1_O_i, L1_I_i, L2_V_i, L2_O_i, L2_I_i
    #
    # Then, compute these results. Since this computation also needs to be done
    # in run_forts.py, make a separate module which contain the functions for
    # doing so.

    def construct_runtime_args_parser(self, parent):
        parser = argparse.ArgumentParser(
            description="Arguments controlling run-time parameters to slice_test",
            parents=[parent])
        return parser


class SlicingExperimentResult(ExperimentResult):
    def __init__(self, outstream, prefetch_quality_results):
        result = prefetch_quality_results
        result["validity"] = find_result(outstream, "Result validity: ", str)
        result["      ifort_s      "] = find_result(
            outstream, "ifort avg run-time [seconds]: ", float)
        result["      slicec_s      "] = find_result(
            outstream, "slicec avg run-time [seconds]: ", float)
        ExperimentResult.__init__(self, result)


def find_result(stream, prefix, conv):
    line = stream.readline()
    while line and line.strip().find(prefix) != 0:
        line = stream.readline()
    if line:
        return conv(line.strip()[len(prefix):])


def main():
    SlicingExperiment().run()


if __name__ == '__main__':
    main()
