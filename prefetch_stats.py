import math

def fraction_valid_prefetches(I, D):
    return max(0.0, 1 - float(D) / I)


def estimated_overshoot(D, T, M):
    return D * T - M


def num_unrolled_iterations(r, U):
    assert r >= U
    return r // U


def time_per_iteration(dim, rs, Us, Tc, To):
    loop_overhead = To
    inner_loop_not_found = True
    num_inner_iter = 1
    for i in range(0, dim):
        num_inner_iter *= rs[i]
        if Us[i] != rs[i] and Us[i] != 1 and inner_loop_not_found:
            loop_overhead = num_unrolled_iterations(rs[i], Us[i]) * To
            inner_loop_not_found = False

    return Us[dim] * num_inner_iter * Tc + loop_overhead

def lowest_timely_prefetch_dist(M, T):
    return int(math.ceil(float(M) / T))
