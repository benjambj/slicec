#include <algorithm>
#include <cassert>
#include <cmath>
#include <iostream>
#include <iterator>
#include <limits>
#include <memory>
#include <sstream>
#include <string>
#include <type_traits>
#include <unordered_map>
#include <vector>

static auto SplitString(const std::string& text, char delim) {
  auto result = std::vector<std::string>{};
  auto textstream = std::istringstream{text};
  std::string token;
  while (std::getline(textstream, token, delim)) {
    auto beg = token.find_first_not_of(" \n\t");
    auto end = token.find_last_not_of(" \n\t");
    if (beg != std::string::npos) {
      result.push_back(token.substr(beg, end - beg + 1));
    }
  }
  return result;
}

struct IndexExpression {
  int32_t Start, Step, End;
};

struct IndexRange {
  uint32_t Start, End;
  int32_t Step;

  explicit IndexRange(const IndexExpression& expr, uint32_t dimLength)
      : Start{expr.Start <= 0 ? dimLength + expr.Start - 1 : expr.Start - 1},
        End{expr.End <= 0 ? dimLength + expr.End - 1 : expr.End - 1},
        Step{expr.Step} {
    End = End - ((End - Start) % Step);
  }

  IndexRange(uint32_t start, uint32_t end, int32_t step)
      : Start{start}, End{end}, Step{step} {}
  IndexRange WithStep(int32_t newStep) const { return {Start, End, newStep}; }
  IndexRange WithStart(uint32_t newStart) const {
    return {newStart, End, Step};
  }
  IndexRange WithEnd(uint32_t newEnd) const { return {Start, newEnd, Step}; }
};

std::ostream& operator<<(std::ostream& s, const IndexRange& c) {
  return s << c.Start << ":" << c.Step << ":" << c.End;
}

struct PrefetchStrategy {
  unsigned Dimension;
  unsigned Distance;
};

struct Args {
  uint32_t TotalSize;
  std::vector<uint32_t> Dims;
  std::vector<IndexRange> IndexRanges;
  std::vector<unsigned> UnrollDepths;
  PrefetchStrategy PrefetchL1;
  PrefetchStrategy PrefetchL2;
  bool UseGather;
};

static auto ParsePositionExpr(const std::string& expr) {
  return std::stoi(expr);
}

static auto ParseStepExpr(const std::string& expr) {
  return std::stoi(expr);
}

static auto ParseIndexExpression(const std::string& indexExprString) {
  if (indexExprString == ":")
    return IndexExpression{.Start = 1, .Step = 1, .End = 0};

  auto components = SplitString(indexExprString, ':');
  auto expr = IndexExpression{};
  switch (components.size()) {
    case 3:
      expr.Start = ParsePositionExpr(components[0]);
      expr.Step = ParseStepExpr(components[2]);
      expr.End = ParsePositionExpr(components[1]);
      return expr;

    case 2:
      expr.Start = ParsePositionExpr(components[0]);
      expr.Step = 1;
      expr.End = ParsePositionExpr(components[1]);
      return expr;

    case 1:
      expr.Start = ParsePositionExpr(components[0]);
      expr.Step = 1;
      expr.End = expr.Start;
      return expr;

    default:
      std::cerr << "Error: the index expression must contain 1-3 operands"
                << std::endl;
      std::exit(EXIT_FAILURE);
  }
}

static auto ParseArgs(int argc, char* argv[]) {
  auto expectedArgc = 9;
  if (argc != expectedArgc) {
    std::cerr << "Error: slicec requires " << (expectedArgc - 1)
              << " arguments, but " << (argc - 1) << " were provided."
              << std::endl;
    std::exit(EXIT_FAILURE);
  }

  auto indexExpr = std::string{argv[2]};
  if (indexExpr.find("M(") != 0 && indexExpr.back() != ')') {
    std::cerr << "Invalid second argument: the index expression must be "
                 "demonstrated by indexing a matrix named M."
              << std::endl
              << "Example: \"M(1:2:1000, 1:1000)\"" << std::endl;
    std::exit(EXIT_FAILURE);
  } else {
    indexExpr = indexExpr.substr(2, indexExpr.size() - 3);
  }

  auto dims = SplitString(argv[1], 'x');
  auto indexExprStrings = SplitString(indexExpr, ',');

  if (dims.size() == 0) {
    std::cerr << "Error: matrix must have at least one dimension." << std::endl;
    std::exit(EXIT_FAILURE);
  }
  if (dims.size() != indexExprStrings.size()) {
    std::cerr << "Error: the number of dimensions must equal the number of "
                 "index expressions."
              << std::endl;
    std::exit(EXIT_FAILURE);
  }

  auto args = Args{};
  args.TotalSize = 1;
  for (const auto& dim : dims) {
    args.Dims.emplace_back(std::stoi(dim));
    auto nextTotalSize =
        static_cast<uint64_t>(args.TotalSize) * args.Dims.back();
    args.TotalSize = static_cast<uint32_t>(nextTotalSize);
    if (args.TotalSize != nextTotalSize) {
      std::cerr << "Error: total matrix size too big." << std::endl;
      std::exit(EXIT_FAILURE);
    }
  }
  for (std::size_t i = 0; i < args.Dims.size(); ++i) {
    auto indexExpr = ParseIndexExpression(indexExprStrings[i]);
    args.IndexRanges.emplace_back(indexExpr, args.Dims[i]);
  }

  for (const auto& depth : SplitString(argv[3], '_')) {
    args.UnrollDepths.push_back(std::stoul(depth));
  }
  args.PrefetchL1.Dimension = std::stoul(argv[4]);
  args.PrefetchL1.Distance = std::stoul(argv[5]);
  args.PrefetchL2.Dimension = std::stoul(argv[6]);
  args.PrefetchL2.Distance = std::stoul(argv[7]);

  args.UseGather = std::string{argv[8]} == "True";

  if (args.UseGather) {
    if (args.UnrollDepths.front() != args.IndexRanges.front().Step) {
      std::cerr << "Error: when using gather instructions, slicec currently "
                   "only supports using the same unroll depth as the inner "
                   "stride\n";
      std::cerr << "(Inner stride is " << args.IndexRanges.front().Step
                << ", but the inner unroll depth requested is "
                << args.UnrollDepths.front() << ")" << std::endl;
      std::exit(1);
    }
    for (auto i = 1u; i < args.UnrollDepths.size(); ++i) {
      if (args.UnrollDepths[i] != 1) {
        std::cerr << "Error: when using gather instructions, slicec currently "
                     "only supports unrolling the innermost dimension.\n";
        std::cerr << "(An unroll depth of " << args.UnrollDepths[i]
                  << " was requested for dimension " << i << ")" << std::endl;
        std::exit(1);
      }
    }
  }

  return args;
}

class Config {
 public:
  static unsigned ElementSize() { return 8; }
  static unsigned VectorElementCount() { return 64 / ElementSize(); }
  static std::string VecRegType() { return "__m512d"; }
  static std::string MaskRegType() { return "__mmask8"; }
  static unsigned NumVectorRegs() { return 32; }
  static unsigned NumMaskRegs() { return 8; }
};

static std::ostream& OpenOutputFile(const Args&) {
  return std::cout;
}

using VarName = std::string;
class Var {
 public:
  static VarName M;
  static VarName M_L1_Offset, M_L2_Offset;
  static VarName R;
  static VarName R_L1_Offset, R_L2_Offset;
  static std::vector<VarName> FaceSizes;
  static std::vector<VarName> n;
  static std::vector<VarName> k;
  static std::vector<VarName> vec;
  static std::vector<VarName> mask;
  static std::vector<VarName> LinearStride;
  static VarName InnerIterCount, RemainderMask, CountMask;
  static VarName L1PrefetchDimCounter;
  static VarName IndexArray;
  static VarName IndexVec;
};
VarName Var::M;
VarName Var::M_L1_Offset, Var::M_L2_Offset;
VarName Var::R;
VarName Var::R_L1_Offset, Var::R_L2_Offset;
std::vector<VarName> Var::FaceSizes;
std::vector<VarName> Var::n;
std::vector<VarName> Var::k;
std::vector<VarName> Var::vec;
std::vector<VarName> Var::mask;
std::vector<VarName> Var::LinearStride;
VarName Var::InnerIterCount, Var::RemainderMask, Var::CountMask;
VarName Var::L1PrefetchDimCounter;
VarName Var::IndexArray;
VarName Var::IndexVec;

static void InitVariableNames(const Args& args) {
  Var::M = "M";
  Var::M_L1_Offset = "M_L1_offset";
  Var::M_L2_Offset = "M_L2_offset";
  Var::R = "R";
  Var::R_L1_Offset = "R_L1_offset";
  Var::R_L2_Offset = "R_L2_offset";
  for (unsigned i = 1; i <= args.Dims.size(); ++i) {
    Var::n.emplace_back("n" + std::to_string(i));
    Var::FaceSizes.emplace_back("face_size_" + std::to_string(i));
    Var::LinearStride.emplace_back("ls" + std::to_string(i));
    Var::k.push_back("k" + std::to_string(i - 1));
  }
  Var::k.push_back("k" + std::to_string(args.Dims.size()));
  for (unsigned i = 0; i < Config::NumVectorRegs(); ++i) {
    Var::vec.emplace_back("vec" + std::to_string(i));
  }
  for (unsigned i = 0; i < Config::NumMaskRegs(); ++i) {
    Var::mask.emplace_back("mask" + std::to_string(i));
  }
  Var::InnerIterCount = "inner_iter_count";
  Var::RemainderMask = "remainder_mask";
  Var::CountMask = "count_mask";
  Var::L1PrefetchDimCounter = "k_L1";
  Var::IndexArray = "indices";
  Var::IndexVec = "index_vec";
}

struct Loop {
  unsigned Level;
  IndexRange IterationRange;
  void EmitLoopHeader(std::ostream& out) const;
};

void Loop::EmitLoopHeader(std::ostream& s) const {
  s << "for (unsigned " << Var::k[Level] << " = " << IterationRange.Start
    << "; " << Var::k[Level] << " <= " << IterationRange.End << "; "
    << Var::k[Level] << " += " << IterationRange.Step << ") {\n";
}

struct OffsetExpression {
 public:
  OffsetExpression(unsigned index) : expr_{std::to_string(index)} {}
  OffsetExpression(int index) : expr_{std::to_string(index)} {}
  OffsetExpression(const VarName& var) : expr_{var} {}
  explicit OffsetExpression(const std::vector<unsigned>& iterationNumbers)
      : expr_{} {
    auto sep = " + ";
    expr_ << "0";
    for (unsigned i = 0; i < iterationNumbers.size(); ++i) {
      if (iterationNumbers[i] != 0) {
        expr_ << sep << iterationNumbers[i] << "*"
              << (i == 0 ? "1" : Var::FaceSizes[i - 1]);
      }
    }
  }
  OffsetExpression(const OffsetExpression& other)
      : expr_{other.expr_.str(), std::ios_base::ate} {}

  template <typename T>
  OffsetExpression operator+(const T& t) {
    OffsetExpression expr{*this};
    expr.expr_ << " + " << t;
    return expr;
  }

 private:
  std::ostringstream expr_;
  friend std::ostream& operator<<(std::ostream&, const OffsetExpression& expr);
};
std::ostream& operator<<(std::ostream& s, const OffsetExpression& expr) {
  return s << expr.expr_.str();
}

struct Address {
  VarName Base;
  OffsetExpression Offset;
  friend std::ostream& operator<<(std::ostream&, const Address&);
};

std::ostream& operator<<(std::ostream& s, const Address& addr) {
  return s << "&" << addr.Base << "[" << addr.Offset << "]";
}

struct Element {
  VarName Base;
  OffsetExpression Offset;
  friend std::ostream& operator<<(std::ostream&, const Element&);
};

std::ostream& operator<<(std::ostream& s, const Element& addr) {
  return s << addr.Base << "[" << addr.Offset << "]";
}

enum class Hint { L1, L1Exclusive, L2, L2Exclusive };

std::ostream& operator<<(std::ostream& s, const Hint& hint) {
  switch (hint) {
    case Hint::L1:
      return s << "_MM_HINT_T0";
    case Hint::L1Exclusive:
      return s << "_MM_HINT_ET0";
    case Hint::L2:
      return s << "_MM_HINT_T1";
    case Hint::L2Exclusive:
      return s << "_MM_HINT_ET1";
  }
}

class MaskSequence;
class Mask {
 public:
  /* Gruesome class design: two completely different roles... */
  unsigned GetValue() const {
    assert(isConstant_);
    return value_;
  }
  VarName GetVarName() const {
    assert(!isConstant_);
    return varName_;
  }
  unsigned GetNumEnabled() const {
    assert(isConstant_);
    return numHigh_;
  }
  bool AreAllDisabled() const { return isConstant_ && GetNumEnabled() == 0; }
  bool AreAllEnabled() const {
    return isConstant_ && GetNumEnabled() == numMax_;
  }
  std::string GetMaskType() const { return "_mmask" + std::to_string(numMax_); }
  friend std::ostream& operator<<(std::ostream&, const Mask&);

  Mask operator&(const Mask& other) {
    assert(isConstant_);
    assert(other.isConstant_);
    assert(numMax_ == other.numMax_);
    return Mask{value_ & other.value_, numMax_};
  }

  explicit Mask(const VarName& var)
      : value_{0},
        numHigh_{0},
        numMax_{Config::VectorElementCount()},
        isConstant_{false},
        varName_{var} {}
  explicit Mask(unsigned val) : Mask(val, Config::VectorElementCount()) {}

 private:
  unsigned value_, numHigh_, numMax_;
  bool isConstant_;
  VarName varName_;
  explicit Mask(unsigned value, unsigned numMax)
      : value_{value},
        numHigh_{0},
        numMax_{numMax},
        isConstant_{true},
        varName_{} {
    while (value != 0) {
      ++numHigh_;
      value &= (value - 1);
    }
  }
  friend Mask operator""_mask8(unsigned long long value);
  friend Mask operator""_mask16(unsigned long long value);
  friend class MaskSequence;
};

Mask operator""_mask8(unsigned long long value) {
  assert(value <= 0xFF);
  return Mask(static_cast<unsigned>(value), 8);
}
Mask operator""_mask16(unsigned long long value) {
  assert(value <= 0xFFFF);
  return Mask(static_cast<unsigned>(value), 16);
}
std::ostream& operator<<(std::ostream& out, const Mask& mask) {
  if (mask.isConstant_)
    return out << mask.GetValue();
  return out << mask.GetVarName();
}

static unsigned GCD(unsigned a, unsigned b) {
  if (b == 0)
    return a;
  return GCD(b, a % b);
}

static unsigned LCM(unsigned a, unsigned b) {
  return (a * b) / GCD(a, b);
}

class MaskSequence {
 public:
  explicit MaskSequence(const IndexRange& innerRange)
      : innerRange_{innerRange} {}

  Mask GetMask(unsigned i) const {
    auto VL = Config::VectorElementCount();
    auto L_i = VL * i;
    auto U_i = std::min(L_i + VL - 1, innerRange_.End - innerRange_.Start);
    auto mask = 0u;
    for (auto j = L_i; j <= U_i; ++j) {
      mask |= (j % innerRange_.Step == 0) << (j - L_i);
    }
    return Mask{mask, VL};
  }

  unsigned GetPeriod() const {
    return innerRange_.Step /
           GCD(innerRange_.Step, Config::VectorElementCount());
  }

 private:
  IndexRange innerRange_;
};

class Instruction {
 private:
  std::stringstream repr_;
  template <typename T>
  Instruction& operator<<(T&& value) {
    repr_ << value;
    return *this;
  }
  Instruction() : repr_{} {}
  Instruction(const Instruction& other) : repr_{other.repr_.str()} {}

 public:
  template <typename T>
  static Instruction LoadVec(const VarName& dest, const Address& address);

  /* ------- LoadUnpackLo ------------- */
  static Instruction LoadUnpackLoVec(const VarName& dest,
                                     const VarName& defaultVal,
                                     const Address& address) {
    return Instruction{} << dest << " = _mm512_loadunpacklo_pd(" << defaultVal
                         << ", " << address << ");";
  }
  static Instruction LoadUnpackLoVec(const VarName& dest,
                                     const Address& address) {
    return LoadUnpackLoVec(dest, dest, address);
  }
  static Instruction LoadUnpackLoVec(const VarName& dest,
                                     const VarName& defaultVal,
                                     const Address& address,
                                     const Mask& mask) {
    if (mask.AreAllEnabled()) {
      return LoadUnpackLoVec(dest, defaultVal, address);
    }
    return Instruction{} << dest << " = _mm512_mask_loadunpacklo_pd("
                         << defaultVal << ", " << std::hex << std::showbase
                         << mask << ", " << address << ");";
  }
  static Instruction LoadUnpackLoVec(const VarName& dest,
                                     const Address& address,
                                     const Mask& mask) {
    return LoadUnpackLoVec(dest, dest, address, mask);
  }

  /* ------- LoadUnpackHi ------------- */
  static Instruction LoadUnpackHiVec(const VarName& dest,
                                     const VarName& defaultVal,
                                     const Address& address) {
    return Instruction{} << dest << " = _mm512_loadunpackhi_pd(" << defaultVal
                         << ", " << address << ");";
  }
  static Instruction LoadUnpackHiVec(const VarName& dest,
                                     const Address& address) {
    return LoadUnpackHiVec(dest, dest, address);
  }
  static Instruction LoadUnpackHiVec(const VarName& dest,
                                     const VarName& defaultVal,
                                     const Address& address,
                                     const Mask& mask) {
    if (mask.AreAllEnabled()) {
      return LoadUnpackHiVec(dest, defaultVal, address);
    }
    return Instruction{} << dest << " = _mm512_mask_loadunpackhi_pd("
                         << defaultVal << ", " << std::hex << std::showbase
                         << mask << ", " << address << ");";
  }
  static Instruction LoadUnpackHiVec(const VarName& dest,
                                     const Address& address,
                                     const Mask& mask) {
    return LoadUnpackHiVec(dest, dest, address, mask);
  }

  /* -------------- Gather -------------- */
  static Instruction Gather(const VarName& dest,
                            const Address& base,
                            const VarName& indices,
                            const unsigned scale) {
    return Instruction{} << dest << " = _mm512_i32logather_pd(" << indices
                         << ", "
                         << "(void*)" << base << ", " << scale << ");";
  }

  static Instruction Gather(const VarName& dest,
                            const VarName& src,
                            const Address& base,
                            const VarName& indices,
                            const unsigned scale,
                            const Mask& mask) {
    return Instruction{} << dest << " = _mm512_mask_i32loextgather_pd(" << src
                         << ", " << mask << ", " << indices << ", " << base
                         << ", _MM_UPCONV_PD_NONE, " << scale
                         << ", _MM_HINT_NONE);";
  }

  static Instruction Gather(const VarName& dest,
                            const Address& base,
                            const VarName& indices,
                            const unsigned scale,
                            const Mask& mask) {
    return Gather(dest, dest, base, indices, scale, mask);
  }

  /* -------------- Store -------------- */
  static Instruction StoreVec(const VarName& src, const Address& address) {
    return Instruction{} << "_mm512_store_pd(" << address << ", " << src
                         << ");";
  }

  /* -------------- PackedStoreLo -------------- */
  static Instruction PackedStoreLoVec(const VarName& src,
                                      const Address& address) {
    return Instruction{} << "_mm512_packstorelo_pd(" << address << ", " << src
                         << ");";
  }
  static Instruction PackedStoreLoVec(const VarName& src,
                                      const Address& address,
                                      const Mask& mask) {
    if (mask.AreAllEnabled()) {
      return PackedStoreLoVec(src, address);
    }
    return Instruction{} << "_mm512_mask_packstorelo_pd(" << address << ", "
                         << std::hex << std::showbase << mask << ", " << src
                         << ");";
  }

  /* -------------- PackedStoreHi -------------- */
  static Instruction PackedStoreHiVec(const VarName& src,
                                      const Address& address) {
    return Instruction{} << "_mm512_packstorehi_pd(" << address << ", " << src
                         << ");";
  }
  static Instruction PackedStoreHiVec(const VarName& src,
                                      const Address& address,
                                      const Mask& mask) {
    if (mask.AreAllEnabled()) {
      return PackedStoreHiVec(src, address);
    }
    return Instruction{} << "_mm512_mask_packstorehi_pd(" << address << ", "
                         << std::hex << std::showbase << mask << ", " << src
                         << ");";
  }

  /* ----------------- Prefetch instructions ------------------ */

  static Instruction Prefetch(const Address& addr, Hint hint) {
    return Instruction{} << "_mm_prefetch((char*)(" << addr << "), " << hint
                         << ");";
  }

  static Instruction CacheLineEvict(const Address& addr, Hint hint) {
    return Instruction{} << "_mm_clevict(" << addr << ", " << hint << ");";
  }

  /* ----------------- Mask instructions ------------------ */
  static Instruction MaskOr(const VarName& src,
                            const VarName& k1,
                            const VarName& k2) {
    return Instruction{} << src << " = _mm512_kor(" << k1 << ", " << k2 << ");";
  }

  static Instruction MaskAnd(const VarName& src,
                             const VarName& k1,
                             const VarName& k2) {
    return Instruction{} << src << " = _mm512_kand(" << k1 << ", " << k2
                         << ");";
  }
  static Instruction MaskAnd(const VarName& src,
                             const VarName& k1,
                             const Mask& k2) {
    return Instruction{} << src << " = _mm512_kand(" << k1 << ", " << k2
                         << ");";
  }

  static Instruction MaskNot(const VarName& src, const VarName& k1) {
    return Instruction{} << src << " = _mm512_knot(" << k1 << ");";
  }

  static Instruction MaskExtract(const VarName& dest,
                                 const VarName src,
                                 unsigned offset) {
    return Instruction{} << dest << " = _mm512_kextract_64(" << src << ", "
                         << offset << ");";
  }

  /* ----------------- Scalar instructions ------------------ */
  template <typename D, typename S>
  static Instruction Assignment(const D& dest, const S& src) {
    return Instruction{} << dest << " = " << src << ";";
  }

  template <typename D, typename S>
  static Instruction Increment(const D& dest, const S& src) {
    return Instruction{} << dest << " += " << src << ";";
  }
  template <typename D, typename S>
  static Instruction Decrement(const D& dest, const S& src) {
    return Instruction{} << dest << " -= " << src << ";";
  }

  friend std::ostream& operator<<(std::ostream&, const Instruction& instr);
};

template <>
Instruction Instruction::LoadVec<double>(const VarName& dest,
                                         const Address& address) {
  return Instruction{} << dest << " = _mm512_load_pd(" << address << ");";
}
template <>
Instruction Instruction::LoadVec<int>(const VarName& dest,
                                      const Address& address) {
  return Instruction{} << dest << " = _mm512_load_epi32(" << address << ");";
}

std::ostream& operator<<(std::ostream& out, const Instruction& instr) {
  return out << instr.repr_.str();
}

class LoopNest {
 public:
  typedef std::vector<Loop>::const_iterator Iterator;
  std::vector<Loop> OuterToInner() const { return loops_; }
  void AddInnerLoop(Loop loop) {
    loop.Level = loops_.size();
    loops_.push_back(loop);
  }

 private:
  std::vector<Loop> loops_;
};

template <typename T>
static void EmitSkeletonPrologue(
    T&& out,
    const Args& args,
    const std::vector<IndexRange>& cacheLineRanges) {
  out << "#include <immintrin.h>\n";
  out << "void do_slicec_slice_(double* " << Var::M << ", ";
  for (const auto& dimParam : Var::n) {
    out << "int* " << dimParam << ", ";
  }
  out << "double* " << Var::R << ") {\n";

  out << "int " << Var::FaceSizes[0] << " = *" << Var::n[0] << ";\n";
  for (unsigned i = 1; i < args.Dims.size(); ++i) {
    out << "int " << Var::FaceSizes[i] << " = *" << Var::n[i] << " * "
        << Var::FaceSizes[i - 1] << ";\n";
  }

  out << "unsigned " << Var::LinearStride[0] << " = " << cacheLineRanges[0].Step
      << ";\n";
  for (unsigned i = 1; i < cacheLineRanges.size(); ++i) {
    auto referenceEnd =
        cacheLineRanges[i - 1].End -
        (cacheLineRanges[i - 1].End - cacheLineRanges[i - 1].Start) %
            cacheLineRanges[i - 1].Step;

    out << "unsigned " << Var::LinearStride[i] << " = "
        << cacheLineRanges[i].Step << "*" << Var::FaceSizes[i - 1] << " - "
        << (referenceEnd + cacheLineRanges[i - 1].Step -
            cacheLineRanges[i - 1].Start)
        << "*" << (i == 1 ? "1" : Var::FaceSizes[i - 2]) << ";\n";
  }

  out << Config::VecRegType() << " ";
  auto sep = "";
  auto realSep = ", ";
  for (const auto& vec : Var::vec) {
    out << sep << vec;
    sep = realSep;
  }
  out << ";\n";

  out << Config::MaskRegType() << " ";
  sep = "";
  realSep = ", ";
  for (const auto& mask : Var::mask) {
    out << sep << mask;
    sep = realSep;
  }
  out << ";\n";

  out << "__int64 " << Var::InnerIterCount << ";\n";
  out << Config::MaskRegType() << " " << Var::RemainderMask << ", "
      << Var::CountMask << ";\n";

  out << "unsigned " << Var::M_L1_Offset << " = 0;\n";
  out << "unsigned " << Var::M_L2_Offset << " = 0;\n";
  out << "unsigned " << Var::R_L1_Offset << " = 0;\n";
  out << "unsigned " << Var::R_L2_Offset << " = 0;\n";
  out << "unsigned " << Var::L1PrefetchDimCounter << " = 0;\n";

  if (args.UseGather) {
    out << "__declspec(aligned(64)) int " << Var::IndexArray << "["
        << Config::VectorElementCount() << "] = {";
    auto innerRange = args.IndexRanges.front();
    sep = "";
    for (unsigned i = 0u; i < Config::VectorElementCount(); ++i) {
      out << sep << i * innerRange.Step;
      sep = ", ";
    }
    out << "};\n";
    out << "__m512i " << Var::IndexVec << ";\n";
    out << Instruction::LoadVec<int>(Var::IndexVec,
                                     Address{Var::IndexArray, 0});
  }

  auto startPoint = std::vector<unsigned>{};
  for (const auto& range : cacheLineRanges) {
    startPoint.push_back(range.Start);
  }
  out << Instruction::Assignment(Var::M,
                                 Address{Var::M, OffsetExpression{startPoint}})
      << std::endl;
}

static unsigned NumElements(const IndexRange& range) {
  return (range.End - range.Start) / range.Step + 1;
}

static unsigned CalculateInnerVectorCount(const Args& args,
                                          unsigned misalignment) {
  auto innerRange = args.IndexRanges[0];

  // If the step is greater than the vector size, then each element will be in
  // its own vector. Otherwise, the vector count is all cache lines within the
  // address span.
  if (static_cast<unsigned>(std::abs(innerRange.Step)) >=
      Config::VectorElementCount())
    return NumElements(innerRange);

  // The misalignment pushes the end access further out in terms of cache line
  // index value, until it reaches the element size where the increased
  // distance
  // of End is mitigated by the fact that there are no longer any elements
  // fetched from the "first" cache line.
  // Actually, it does not really make sense for "misalignment" to be greater
  // than the element size.
  return std::ceil((innerRange.End - innerRange.Start + 1 +
                    (misalignment % Config::ElementSize())) /
                   static_cast<double>(Config::ElementSize()));
}

struct LoopUnroll {
  explicit LoopUnroll(unsigned mainLoopIter, unsigned remainderLoopIter)
      : MainLoopIterations{mainLoopIter},
        RemainderLoopIterations{remainderLoopIter} {}
  unsigned MainLoopIterations;
  unsigned RemainderLoopIterations;
};

struct UnrollPrefetchConfig {
  std::vector<LoopUnroll> UnrollDepths;
  PrefetchStrategy PrefetchL1, PrefetchL2;
};

static inline double FractionValidPrefetches(unsigned numLoopIterations,
                                             unsigned numIterPrefetchedAhead) {
  return std::max(
      0.0, 1 - static_cast<double>(numIterPrefetchedAhead) / numLoopIterations);
}

static inline unsigned EstimatedOvershoot(unsigned numIterPrefetchedAhead,
                                          unsigned timePerIter,
                                          unsigned expectedMemoryLatency) {
  return numIterPrefetchedAhead * timePerIter - expectedMemoryLatency;
}

template <typename T>
static void EmitLoopHeader(T&& out,
                           const IndexRange& range,
                           const VarName& loopVar) {
  out << "for (unsigned " << loopVar << " = " << range.Start << "; " << loopVar
      << " <= " << range.End << "; " << loopVar << " += " << range.Step
      << ") {\n";
}
template <typename T>
static void EmitLoopHeaderWithoutInit(T&& out,
                                      const IndexRange& range,
                                      const VarName& loopVar) {
  out << "for (; " << loopVar << " <= " << range.End << "; " << loopVar
      << " += " << range.Step << ") {\n";
}

static IndexRange CalculateUnrolledMainLoopRange(
    const IndexRange& innerRange,
    const LoopUnroll& innerUnroll) {
  return innerRange.WithStep(innerRange.Step * innerUnroll.MainLoopIterations)
      .WithEnd(innerRange.End -
               innerUnroll.RemainderLoopIterations * innerRange.Step);
}

class IterationSpace {
 public:
  explicit IterationSpace(const std::vector<IndexRange>& indexRanges,
                          // This maskSequence is constructed with an
                          // element-wise index range. We cannot construct it
                          // from the indexRange vector parameter, since this is
                          // a cache line range.
                          const MaskSequence& maskSequence)
      : depth_{static_cast<unsigned>(indexRanges.size())},
        lowerBounds_(indexRanges.size(), 0),
        upperBounds_(indexRanges.size(), 0),
        steps_(indexRanges.size(), 0),
        maskSequence_{maskSequence} {
    for (unsigned i = 0; i < depth_; ++i) {
      lowerBounds_[i] = indexRanges[i].Start;
      upperBounds_[i] = indexRanges[i].End;
      steps_[i] = indexRanges[i].Step;
    }
  }

  class IterationVector {
   public:
    std::vector<unsigned> GetIterationNumbers() const {
      return iterationNumbers_;
    }
    IterationVector operator*() const { return *this; }

    std::vector<unsigned> GetShiftedIterationNumbers() const {
      auto numbers = iterationNumbers_;
      for (auto i = 0u; i < numbers.size(); ++i) {
        numbers[i] -= space_->lowerBounds_[i];
      }
      return numbers;
    }

    Mask GetMask() const {
      auto VL = Config::VectorElementCount();
      return space_->maskSequence_.GetMask(iterationNumbers_[0] / VL);
    }

    IterationVector& operator++() {
      iterationNumbers_[0] += space_->steps_[0];
      for (unsigned dim = 0; dim < iterationNumbers_.size() - 1 &&
                             iterationNumbers_[dim] > space_->upperBounds_[dim];
           ++dim) {
        iterationNumbers_[dim] = space_->lowerBounds_[dim];
        iterationNumbers_[dim + 1] += space_->steps_[dim + 1];
      }
      return *this;
    }
    IterationVector operator++(int) {
      auto copy = *this;
      ++copy;
      return copy;
    }
    IterationVector& operator+=(unsigned n) {
      for (unsigned i = 0; i < n; ++i)
        ++*this;
      return *this;
    }
    IterationVector operator+(unsigned n) {
      auto copy = *this;
      copy += n;
      return copy;
    }

    bool operator!=(const IterationVector& other) {
      if (other.space_ != space_)
        return true;
      for (unsigned i = 0; i < iterationNumbers_.size(); ++i) {
        if (iterationNumbers_[i] != other.iterationNumbers_[i]) {
          return true;
        }
      }
      return false;
    }
    bool operator==(const IterationVector& other) { return !(*this != other); }

   private:
    std::vector<unsigned> iterationNumbers_;
    friend class IterationSpace;
    const IterationSpace* space_;
    explicit IterationVector(const std::vector<unsigned>& iterationNumbers,
                             const IterationSpace* space)
        : iterationNumbers_(iterationNumbers), space_{space} {}
  };

  IterationVector begin() const {
    return IterationVector(std::vector<unsigned>(lowerBounds_), this);
  }
  IterationVector end() const {
    for (auto i = 0u; i < depth_; ++i) {
      if (lowerBounds_[i] > upperBounds_[i]) {
        return IterationVector{lowerBounds_, this};
      }
    }
    auto firstOutOfBoundsIndex = lowerBounds_;
    firstOutOfBoundsIndex.back() = upperBounds_.back() + steps_.back();
    return IterationVector(firstOutOfBoundsIndex, this);
  }

  bool IsEmpty() const { return GetNumPoints() == 0; }
  unsigned GetNumPoints() const {
    auto numPoints = 1u;
    for (auto i = 0u; i < depth_; ++i) {
      numPoints *= (upperBounds_[i] - lowerBounds_[i] + steps_[i]) / steps_[i];
    }
    return numPoints;
  }

 private:
  unsigned depth_;
  std::vector<uint32_t> lowerBounds_, upperBounds_;
  std::vector<int32_t> steps_;
  MaskSequence maskSequence_;
};

// XXX: This function has turned bad: it does too many things!
// In particular, the remainder mask bit is a sore thumb...
// Haha now with gather instructions added the function is nightmarish...
template <typename T>
static Mask EmitLoopBody(T&& out,
                         const IterationSpace& unrolledSpace,
                         bool useRemainderMask,
                         bool useGather,
                         unsigned innerStride) {
  if (useRemainderMask) {
    out << Instruction::MaskExtract(Var::CountMask, Var::InnerIterCount, 3)
        << std::endl;
  }
  if (useRemainderMask && useGather) {
    out << Instruction::MaskNot(Var::CountMask, Var::CountMask) << std::endl;
    out << Instruction::MaskOr(Var::CountMask, Var::CountMask,
                               Var::RemainderMask)
        << std::endl;
  }

  auto R_offset = 0u;
  auto finalPointMask = 0_mask8;
  // If we use gather instructions instead of single-line loads, we need a
  // register to store the current mask index in.
  auto curIndexReg = Var::IndexVec;
  // auto curIndexNum = 0u;
  for (auto iv = std::begin(unrolledSpace), oob = std::end(unrolledSpace);
       iv != oob;) {
    auto ivCopy = iv;
    auto curGatherDestReg = begin(Var::vec);
    for (unsigned j = 0; j < Config::NumVectorRegs() - useGather && iv != oob;
         ++j, ++iv) {
      auto iterationNumbers = iv.GetShiftedIterationNumbers();
      if (!useGather) {
        out << Instruction::LoadUnpackLoVec(
                   Var::vec[j],
                   Address{Var::M, OffsetExpression{iterationNumbers}})
            << std::endl;
      }
      out << Instruction::CacheLineEvict(
                 Address{Var::M, OffsetExpression{iterationNumbers}}, Hint::L1)
          << std::endl;
      out << Instruction::Prefetch(
                 Address{Var::M, OffsetExpression{iterationNumbers} +
                                     OffsetExpression{Var::M_L1_Offset}},
                 Hint::L1)
          << std::endl;
      if (!useGather) {
        out << Instruction::LoadUnpackHiVec(
                   Var::vec[j],
                   Address{Var::M, OffsetExpression{iterationNumbers} + 8})
            << std::endl;
      }
      out << Instruction::Prefetch(
                 Address{Var::M, OffsetExpression{iterationNumbers} +
                                     OffsetExpression{Var::M_L2_Offset}},
                 Hint::L2)
          << std::endl;
      if (useGather) {
        if ((j + 1) % innerStride == 0) {
          if (useRemainderMask) {
            out << Instruction::Gather(*curGatherDestReg++, Address{Var::M, 0},
                                       curIndexReg, Config::ElementSize(),
                                       Mask{Var::CountMask})
                << std::endl;
          } else {
            out << Instruction::Gather(*curGatherDestReg++, Address{Var::M, 0},
                                       curIndexReg, Config::ElementSize())
                << std::endl;
          }
        }
      }
    }
    if (useRemainderMask && !useGather) {
      out << Instruction::MaskNot(Var::CountMask, Var::CountMask) << std::endl;
      out << Instruction::MaskOr(Var::CountMask, Var::CountMask,
                                 Var::RemainderMask)
          << std::endl;
    }

    auto curStoreSrcReg = begin(Var::vec);
    iv = ivCopy;
    for (unsigned j = 0; j < Config::NumVectorRegs() - useGather && iv != oob;
         ++j, ++iv) {
      auto isFinalVector = (iv + 1) == oob;
      auto iterationNumbers = iv.GetShiftedIterationNumbers();
      auto mask = iv.GetMask();
      if (isFinalVector && useRemainderMask && !useGather) {
        out << Instruction::MaskAnd(Var::CountMask, Var::CountMask, mask)
            << std::endl;
        mask = Mask{Var::CountMask};
      }
      if (!useGather) {
        out << Instruction::PackedStoreLoVec(Var::vec[j],
                                             Address{Var::R, R_offset}, mask)
            << std::endl;
      }
      out << Instruction::Prefetch(
                 Address{Var::R, OffsetExpression{R_offset} +
                                     OffsetExpression{Var::R_L1_Offset}},
                 Hint::L1Exclusive)
          << std::endl;
      if (!useGather) {
        out << Instruction::PackedStoreHiVec(
                   Var::vec[j], Address{Var::R, R_offset + 8}, mask)
            << std::endl;
      }
      out << Instruction::Prefetch(
                 Address{Var::R, OffsetExpression{R_offset} +
                                     OffsetExpression{Var::R_L2_Offset}},
                 Hint::L2Exclusive)
          << std::endl;
      if (useGather) {
        if ((j + 1) % innerStride == 0) {
          // Store the next line. We will have loaded a full line every
          // innerStride iterations. We still need to use unaligned stores,
          // since we may not store a full vector's worth of data in the
          // remainder loop.
          assert(curStoreSrcReg != curGatherDestReg);
          if (useRemainderMask) {
            out << Instruction::PackedStoreLoVec(*curStoreSrcReg,
                                                 Address{Var::R, R_offset},
                                                 Mask{Var::CountMask})
                << std::endl;
            out << Instruction::PackedStoreHiVec(*curStoreSrcReg++,
                                                 Address{Var::R, R_offset + 8},
                                                 Mask{Var::CountMask})
                << std::endl;
          } else {
            out << Instruction::PackedStoreLoVec(*curStoreSrcReg,
                                                 Address{Var::R, R_offset})
                << std::endl;
            out << Instruction::PackedStoreHiVec(*curStoreSrcReg++,
                                                 Address{Var::R, R_offset + 8})
                << std::endl;
          }
          R_offset += 8;
        }
      } else {
        R_offset += iv.GetMask().GetNumEnabled();
      }
      finalPointMask = iv.GetMask();
    }
    // Assert that we have store all loaded registers.
    assert(curStoreSrcReg == curGatherDestReg);
  }
  // if (useGather && curIndexNum > 0) {
  //   // Emit one final gather and store. This is used when the loop iteration
  //   // count is not a multiple of the mask period and we need a remainder
  //   // loop.
  //   auto remMask = Mask((1 << curIndexNum) - 1);
  //   out << Instruction::Gather(Var::vec[0], Address{Var::M, 0}, curIndexReg,
  //                              Config::ElementSize(), remMask)
  //       << std::endl;
  //   out << Instruction::PackedStoreLoVec(Var::vec[0], Address{Var::R,
  //   R_offset},
  //                                        remMask)
  //       << std::endl;
  //   out << Instruction::PackedStoreHiVec(Var::vec[0],
  //                                        Address{Var::R, R_offset + 8},
  //                                        remMask)
  //       << std::endl;
  //   R_offset += curIndexNum;
  // }

  auto oneOffIterationNumbers =
      std::end(unrolledSpace).GetShiftedIterationNumbers();
  out << Instruction::Assignment(
             Var::M, Address{Var::M, OffsetExpression{oneOffIterationNumbers}})
      << std::endl;
  out << Instruction::Assignment(Var::R, Address{Var::R, R_offset})
      << std::endl;

  return finalPointMask;
}

template <typename T>
static void EmitLoopFooter(T&& out, unsigned i) {
  auto M = Var::LinearStride.size() - 1;
  out << Instruction::Assignment(Var::M,
                                 Address{Var::M, Var::LinearStride[M - i]})
      << "\n}\n";
}

struct UnrolledIterationSpace {
  IterationSpace MainSpace, RemainderSpace;
  std::vector<IndexRange> OuterLoopRanges;
  IndexRange MainLoopRange;
  bool InnermostPartiallyUnrolled;
  Mask RemainderMask;
  bool LastDimCompletelyUnrolled;
  unsigned NumMaskedOutRem;
  unsigned MainUnrollDepth;
  unsigned FinalMainIterLinesWithData;

  explicit UnrolledIterationSpace(
      const IterationSpace& mainSpace,
      const IterationSpace& remainderSpace,
      const std::vector<IndexRange>& outerLoopRanges,
      const IndexRange& mainLoopRange,
      bool innermostPartiallyUnrolled,
      Mask remainderMask,
      bool lastDimCompletelyUnrolled,
      unsigned numMaskedOutRem,
      unsigned mainUnrollDepth,
      unsigned finalMainIterLinesWithData)
      : MainSpace{mainSpace},
        RemainderSpace{remainderSpace},
        OuterLoopRanges{outerLoopRanges},
        MainLoopRange{mainLoopRange},
        InnermostPartiallyUnrolled{innermostPartiallyUnrolled},
        RemainderMask{remainderMask},
        LastDimCompletelyUnrolled{lastDimCompletelyUnrolled},
        NumMaskedOutRem{numMaskedOutRem},
        MainUnrollDepth{mainUnrollDepth},
        FinalMainIterLinesWithData{finalMainIterLinesWithData} {}
};

template <typename T>
static void EmitCopyLoop(T&& out,
                         const UnrolledIterationSpace& space,
                         bool useGather,
                         unsigned innerStride) {
  for (unsigned i = 0; i < space.OuterLoopRanges.size(); ++i) {
    EmitLoopHeader(out, space.OuterLoopRanges[i], Var::k[i]);
  }

  auto useLoop = space.MainLoopRange.End >=
                 space.MainLoopRange.Start + space.MainLoopRange.Step;
  auto handleRemainderInMain =
      ((space.InnermostPartiallyUnrolled && space.RemainderSpace.IsEmpty()) ||
       useGather) &&
      // XXX: This could be (space.RemainderMask &
      // finalMask) != 0, I think...
      space.RemainderMask.GetValue() != 0;

  if (useLoop) {
    if (handleRemainderInMain) {
      out << Instruction::Assignment(Var::InnerIterCount,
                                     NumElements(space.MainLoopRange) - 1)
          << std::endl;
      out << Instruction::Assignment(Var::RemainderMask, space.RemainderMask)
          << std::endl;
    }
    auto mainLoopDepth = space.OuterLoopRanges.size();
    EmitLoopHeader(out, space.MainLoopRange, Var::k[mainLoopDepth]);

    if (handleRemainderInMain) {
      out << Instruction::Decrement(Var::InnerIterCount, 1) << std::endl;
    }
  }

  auto finalMask =
      EmitLoopBody(out, space.MainSpace, useLoop && handleRemainderInMain,
                   useGather && (innerStride > 1 || handleRemainderInMain), innerStride);

  if (useLoop) {
    out << "}\n";
  }
  if (handleRemainderInMain) {
    auto extra_R_offset = space.NumMaskedOutRem;
    out << Instruction::Assignment(
               Var::R, Address{Var::R, -static_cast<int>(extra_R_offset)})
        << std::endl;
    if (useGather) {
      // May also have to adjust M, since the unroll factor may have increased M
      // more than one cache line past the final line.
      auto extra_M_offset =
          Config::VectorElementCount() *
          (space.MainUnrollDepth - space.FinalMainIterLinesWithData);
      if (extra_M_offset > 0) {
        out << Instruction::Assignment(
                   Var::M, Address{Var::M, -static_cast<int>(extra_M_offset)})
            << std::endl;
      }
    }
  } else {
    EmitLoopBody(out, space.RemainderSpace, false, useGather && innerStride > 1,
                 innerStride);
  }

  for (unsigned i = space.OuterLoopRanges.size(); i > 0; --i) {
    EmitLoopFooter(out, i - 1);
  }
}

template <typename T>
static void EmitL1PrefetchPrologue(T&& out,
                                   const std::vector<IndexRange>& l1LoopRanges,
                                   const unsigned prefetchUnrollDepth,
                                   const unsigned prefetchRemainder,
                                   const unsigned prefetchRemainderStartMask,
                                   const MaskSequence& maskSequence) {
  for (auto i = 0u; i < l1LoopRanges.size(); ++i) {
    EmitLoopHeader(out, l1LoopRanges[i], Var::k[i]);
  }

  auto M_L1_Offset = 0u, R_Offset = 0u;
  auto VL = Config::VectorElementCount();
  for (auto i = 0u; i < prefetchUnrollDepth; ++i) {
    out << Instruction::Prefetch(
               Address{Var::M,
                       OffsetExpression{Var::M_L1_Offset} + M_L1_Offset},
               Hint::L1)
        << std::endl;
    out << Instruction::Prefetch(
               Address{Var::R, OffsetExpression{Var::R_L1_Offset} + R_Offset},
               Hint::L1Exclusive)
        << std::endl;

    M_L1_Offset += VL;
    R_Offset += maskSequence.GetMask(i).GetNumEnabled();
  }
  out << Instruction::Increment(Var::M_L1_Offset, M_L1_Offset) << std::endl;
  out << Instruction::Increment(Var::R_L1_Offset, R_Offset) << std::endl;
  out << "}\n";

  auto M_L1_Offset_remainder = 0u, R_Offset_remainder = 0u;
  for (auto i = 0u; i < prefetchRemainder; ++i) {
    out << Instruction::Prefetch(
               Address{Var::M,
                       OffsetExpression{Var::M_L1_Offset} + M_L1_Offset},
               Hint::L1)
        << std::endl;
    out << Instruction::Prefetch(
               Address{Var::R, OffsetExpression{Var::R_L1_Offset} + R_Offset},
               Hint::L1Exclusive)
        << std::endl;

    M_L1_Offset += VL;
    M_L1_Offset_remainder += VL;
    auto numCopied =
        maskSequence.GetMask(i + prefetchRemainderStartMask).GetNumEnabled();
    R_Offset += numCopied;
    R_Offset_remainder += numCopied;
  }
  if (prefetchRemainder > 0) {
    out << Instruction::Increment(Var::M_L1_Offset, M_L1_Offset_remainder)
        << std::endl;
    out << Instruction::Increment(Var::R_L1_Offset, R_Offset_remainder)
        << std::endl;
  }

  for (auto i = l1LoopRanges.size() - 1; i > 0; --i) {
    auto M = l1LoopRanges.size();
    out << Instruction::Increment(Var::M_L1_Offset, Var::LinearStride[M - i])
        << std::endl;
    out << "}\n";
  }
}

template <typename T>
static void EmitL2PrefetchPrologue(T&& out,
                                   const std::vector<IndexRange>& l2LoopRanges,
                                   const unsigned prefetchUnrollDepth,
                                   const unsigned prefetchRemainder,
                                   const unsigned prefetchRemainderStartMask,
                                   const MaskSequence& maskSequence,
                                   const PrefetchStrategy& prefetchL1,
                                   const PrefetchStrategy& prefetchL2) {
  for (auto i = 0u; i < l2LoopRanges.size(); ++i) {
    if (i == prefetchL2.Dimension - prefetchL1.Dimension) {
      EmitLoopHeaderWithoutInit(out, l2LoopRanges[i],
                                Var::L1PrefetchDimCounter);
    } else {
      EmitLoopHeader(out, l2LoopRanges[i], Var::k[i]);
    }
  }

  auto M_L2_Offset = 0u, R_Offset = 0u;
  auto VL = Config::VectorElementCount();
  for (auto i = 0u; i < prefetchUnrollDepth; ++i) {
    out << Instruction::Prefetch(
               Address{Var::M,
                       OffsetExpression{Var::M_L2_Offset} + M_L2_Offset},
               Hint::L2)
        << std::endl;
    out << Instruction::Prefetch(
               Address{Var::R, OffsetExpression{Var::R_L2_Offset} + R_Offset},
               Hint::L2Exclusive)
        << std::endl;

    M_L2_Offset += VL;
    R_Offset += maskSequence.GetMask(i).GetNumEnabled();
  }
  out << Instruction::Increment(Var::M_L2_Offset, M_L2_Offset) << std::endl;
  out << Instruction::Increment(Var::R_L2_Offset, R_Offset) << std::endl;
  out << "}\n";

  auto M_L2_Offset_remainder = 0u, R_Offset_remainder = 0u;
  for (auto i = 0u; i < prefetchRemainder; ++i) {
    out << Instruction::Prefetch(
               Address{Var::M,
                       OffsetExpression{Var::M_L2_Offset} + M_L2_Offset},
               Hint::L2)
        << std::endl;
    out << Instruction::Prefetch(
               Address{Var::R, OffsetExpression{Var::R_L2_Offset} + R_Offset},
               Hint::L2Exclusive)
        << std::endl;

    M_L2_Offset += VL;
    M_L2_Offset_remainder += VL;
    auto numCopied =
        maskSequence.GetMask(i + prefetchRemainderStartMask).GetNumEnabled();
    R_Offset += numCopied;
    R_Offset_remainder += numCopied;
  }
  if (prefetchRemainder > 0) {
    out << Instruction::Increment(Var::M_L1_Offset, M_L2_Offset_remainder)
        << std::endl;
    out << Instruction::Increment(Var::R_L1_Offset, R_Offset_remainder)
        << std::endl;
  }

  for (auto i = l2LoopRanges.size() - 1; i > 0; --i) {
    auto M = l2LoopRanges.size();
    out << Instruction::Increment(Var::M_L2_Offset, Var::LinearStride[M - i])
        << std::endl;
    if (i == prefetchL2.Dimension - prefetchL1.Dimension) {
      out << Instruction::Assignment(Var::L1PrefetchDimCounter,
                                     l2LoopRanges[i].Start)
          << std::endl;
    }
    out << "}\n";
  }
}

template <typename T>
static void EmitPrefetchPrologue(
    T&& out,
    const std::vector<LoopUnroll>& unrollDepths,
    const PrefetchStrategy& PrefetchL1,
    const PrefetchStrategy& PrefetchL2,
    const std::vector<IndexRange>& cacheLineIndexRanges,
    const MaskSequence& maskSequence) {
  // Minimal unrolling here!
  // - Get minimal inner unroll (but we need some, for masks used to calc.
  // R_offset)
  // - Generate tight loop with prefetches to L1
  // - Generate tight loop with prefetches to L2, which start where the L1
  // loop
  // left off.

  auto l1LoopRanges = std::vector<IndexRange>(
      cacheLineIndexRanges.rend() - PrefetchL1.Dimension - 1,
      cacheLineIndexRanges.rend());
  auto l2LoopRanges = std::vector<IndexRange>(
      cacheLineIndexRanges.rend() - PrefetchL2.Dimension - 1,
      cacheLineIndexRanges.rend());

  auto numL1DimIter =
      PrefetchL1.Distance *
      ((PrefetchL1.Dimension >= unrollDepths.size())
           ? 1u
           : unrollDepths[PrefetchL1.Dimension].MainLoopIterations);
  auto outerL1Range = l1LoopRanges.front();
  l1LoopRanges.front() = outerL1Range.WithEnd(
      outerL1Range.Start + (numL1DimIter - 1) * outerL1Range.Step);
  auto numL2DimIter =
      PrefetchL2.Distance *
      ((PrefetchL2.Dimension >= unrollDepths.size())
           ? 1u
           : unrollDepths[PrefetchL2.Dimension].MainLoopIterations);
  auto outerL2Range = l2LoopRanges.front();
  l2LoopRanges.front() = outerL2Range.WithEnd(
      outerL2Range.Start + (numL2DimIter - 1) * outerL2Range.Step);

  auto innerCacheLineRange = l1LoopRanges.back();
  auto numInner = NumElements(innerCacheLineRange);

  auto prefetchUnrollDepth = maskSequence.GetPeriod();
  auto prefetchRemainder = numInner % prefetchUnrollDepth;
  auto VL = Config::VectorElementCount();
  auto prefetchRemainderStart = numInner - prefetchRemainder;
  auto prefetchRemainderStartMask = prefetchRemainderStart / VL;

  l1LoopRanges.back().Step *= prefetchUnrollDepth;
  l2LoopRanges.back().Step *= prefetchUnrollDepth;

  EmitL1PrefetchPrologue(out, l1LoopRanges, prefetchUnrollDepth,
                         prefetchRemainder, prefetchRemainderStartMask,
                         maskSequence);
  out << Instruction::Assignment(Var::M_L2_Offset, Var::M_L1_Offset)
      << std::endl;
  out << Instruction::Assignment(Var::R_L2_Offset, Var::R_L1_Offset)
      << std::endl;
  out << Instruction::Assignment(
             Var::L1PrefetchDimCounter,
             l1LoopRanges.front().End + l1LoopRanges.front().Step)
      << std::endl;

  EmitL2PrefetchPrologue(out, l2LoopRanges, prefetchUnrollDepth,
                         prefetchRemainder, prefetchRemainderStartMask,
                         maskSequence, PrefetchL1, PrefetchL2);
}

static UnrolledIterationSpace CreateUnrolledIterationSpace(
    const std::vector<IndexRange>& indexRanges,
    const std::vector<LoopUnroll>& unrollDepths,
    const MaskSequence& maskSequence,
    const MaskSequence& remainderMaskSequence,
    bool useGather,
    const IndexRange& innerElementRange) {
  auto M = indexRanges.size();
  auto N = unrollDepths.size();
  auto innerRange = indexRanges[N - 1];
  auto innerUnroll = unrollDepths.back();
  auto unrolledLoopRanges = std::vector<IndexRange>(
      begin(indexRanges), begin(indexRanges) + unrollDepths.size() - 1);

  unrolledLoopRanges.push_back(innerRange.WithEnd(
      innerRange.Start +
      (innerUnroll.MainLoopIterations - 1) * innerRange.Step));
  auto mainSpace = IterationSpace{unrolledLoopRanges, maskSequence};
  unrolledLoopRanges.pop_back();

  unrolledLoopRanges.push_back(innerRange.WithEnd(
      innerRange.Start +
      (innerUnroll.RemainderLoopIterations - 1) * innerRange.Step));
  auto remainderSpace =
      IterationSpace{unrolledLoopRanges, remainderMaskSequence};

  auto nOuterLoops = M - N;
  auto outerLoopRanges = std::vector<IndexRange>(
      indexRanges.rbegin(), indexRanges.rbegin() + nOuterLoops);
  auto mainLoopRange = CalculateUnrolledMainLoopRange(innerRange, innerUnroll);

  auto r1 = NumElements(innerElementRange);
  auto innermostPartiallyUnrolled = nOuterLoops == M - 1;
  auto remainderMask =
      useGather ? Mask{(1u << (r1 % Config::VectorElementCount())) - 1}
                : Mask{(1u << ((innerRange.End - innerRange.Start + 1) %
                               innerRange.Step)) -
                       1};

  auto elemsPerNormalIter =
      useGather ? Config::VectorElementCount()
                : Config::VectorElementCount() /
                      GCD(Config::VectorElementCount(), innerElementRange.Step);
  auto numMaskedOutRem =
      elemsPerNormalIter * static_cast<unsigned>(std::ceil(
                               static_cast<double>(r1) / elemsPerNormalIter)) -
      r1;

  auto lastDimCompletelyUnrolled = innerUnroll.MainLoopIterations == 1 &&
                                   innerUnroll.RemainderLoopIterations == 0;

  auto mainUnrollDepth = innerUnroll.MainLoopIterations;
  auto finalIterLinesWithData = NumElements(innerRange) % mainUnrollDepth;
  if (finalIterLinesWithData == 0)
    finalIterLinesWithData = mainUnrollDepth;

  return UnrolledIterationSpace{mainSpace,
                                remainderSpace,
                                outerLoopRanges,
                                mainLoopRange,
                                innermostPartiallyUnrolled,
                                remainderMask,
                                lastDimCompletelyUnrolled,
                                numMaskedOutRem,
                                mainUnrollDepth,
                                finalIterLinesWithData};
}

static UnrollPrefetchConfig GetUnrollAndPrefetchConfig(
    const Args& args,
    const std::vector<IndexRange>& cacheLineRanges) {
  auto unrollConfig = std::vector<LoopUnroll>{};
  for (auto i = 0u; i < args.UnrollDepths.size(); ++i) {
    auto r_i = NumElements(cacheLineRanges[i]);
    if (args.UnrollDepths[i] == r_i) {
      unrollConfig.emplace_back(args.UnrollDepths[i], 0);
    } else {
      if (args.UseGather) {
        unrollConfig.emplace_back(args.UnrollDepths[i], 0);
      } else {
        unrollConfig.emplace_back(args.UnrollDepths[i],
                                  r_i % args.UnrollDepths[i]);
      }
      break;
    }
  }
  return UnrollPrefetchConfig{unrollConfig, args.PrefetchL1, args.PrefetchL2};
}

template <typename T>
static void EmitSlicingCode(
    T&& out,
    const Args& args,
    const std::vector<IndexRange>& cacheLineIndexRanges) {
  auto innerRange = args.IndexRanges.front();
  auto innerCacheRange = cacheLineIndexRanges.front();
  auto maskSequence = MaskSequence{innerRange};

  auto unrollPrefetchConfig =
      GetUnrollAndPrefetchConfig(args, cacheLineIndexRanges);

  auto remainderMaskSequence = maskSequence;
  auto innerUnroll = unrollPrefetchConfig.UnrollDepths.front();
  if (innerUnroll.RemainderLoopIterations != 0) {
    auto nRemainderIterations = innerUnroll.RemainderLoopIterations;
    auto nMainIterations = NumElements(innerCacheRange) - nRemainderIterations;
    auto remainderRange = innerRange.WithEnd(
        innerRange.End - nMainIterations * innerCacheRange.Step);
    remainderMaskSequence = MaskSequence{remainderRange};
  }

  const auto unrolledIS = CreateUnrolledIterationSpace(
      cacheLineIndexRanges, unrollPrefetchConfig.UnrollDepths, maskSequence,
      remainderMaskSequence, args.UseGather, innerRange);

  EmitPrefetchPrologue(
      out, unrollPrefetchConfig.UnrollDepths, unrollPrefetchConfig.PrefetchL1,
      unrollPrefetchConfig.PrefetchL2, cacheLineIndexRanges, maskSequence);
  EmitCopyLoop(out, unrolledIS, args.UseGather, innerRange.Step);
}

template <typename T>
static void EmitSkeletonEpilogue(T&& out) {
  out << "}\n";
}

int main(int argc, char* argv[]) {
  auto args = ParseArgs(argc, argv);

  InitVariableNames(args);

  auto&& out = OpenOutputFile(args);

  auto cacheLineIndexRanges = args.IndexRanges;

  // FIXME: Should be bound from below at 8, but if step is above 8 then we
  // want to keep the original step (in order to support steps > 8).
  cacheLineIndexRanges.front().Step = 8;

  EmitSkeletonPrologue(out, args, cacheLineIndexRanges);

  EmitSlicingCode(out, args, cacheLineIndexRanges);

  EmitSkeletonEpilogue(out);

  return 0;
}
