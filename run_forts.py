#!/usr/bin/env python

from __future__ import print_function

from prefetch_stats import *

import argparse
import functools
import itertools
import math
import os
import re
import subprocess
import sys


def compute_final_dim_size(a, b, s, n, size):
    assert len(n) == len(b) and len(b) + 1 == len(s) and len(s) == len(a), \
        "Incompatible lengths of arrays"

    factor = size
    for i in range(len(n)):
        # print(b[i], n[i], a[i], s[i])
        factor /= (min(b[i], n[i]) - a[i]) // s[i] + 1
    return int(round(a[-1] + s[-1] * (factor - 1)))


class FixedUnrollStrategy:
    def __init__(self, depth):
        self.depth = depth

    def get_depth(self, point, num_dims):
        return self.depth


class DynamicUnrollStrategy(object):
    def __init__(self, opts):
        # Opts can potentially indicate whether or not we take into account:
        # - Fraction of inner loop iterations with useful prefetches
        # - Prefetch distance precision
        # - Mask period: round up, round down, fully unroll, ...
        # - The minimum unroll depth.
        # - Remainder loop policy: avoid, avoid-below-threshold-{fraction,size}, keep, ...
        # - Extra unfilled column management..

        pass

    def get_depth(self, point, num_dims):
        n, a, r, s, b, size, _, elem_size = split_point(point, num_dims)
        # And then calculate something.


def valid_unroll_strategy(strategy):
    fixed_strat_pattern = re.compile("^fixed_(\\d+)$")

    fixed_match = fixed_strat_pattern.match(strategy)
    if fixed_match:
        return FixedUnrollStrategy(int(fixed_match.group(1)))
    if strategy.startswith("dynamic_"):
        return DynamicUnrollStrategy(strategy.split("_")[1:])
    else:
        raise argparse.ArgumentTypeError(
            strategy + " is not a recognized unroll strategy")


def split_point(point, num_dims):
    n = point[0:num_dims - 1]
    a = point[num_dims - 1:2 * num_dims - 1]
    r = point[2 * num_dims - 1:3 * num_dims - 2]
    s = point[3 * num_dims - 2:4 * num_dims - 2]
    b = [a[i] + (r[i] - 1) * s[i] for i in range(num_dims - 1)]
    # print(len(args.n), len(args.r), len(args.s), len(args.a))
    # print(a)
    # print(b)
    # print(s)
    size = point[4 * num_dims - 2]
    elem_size = point[4 * num_dims - 1]
    return (n, a, r, s, b, sive, unroll_strat, elem_size)


def calculate_mask_period(s, VL):
    def gcd(a, b):
        if b == 0:
            return a
        return gcd(b, a % b)

    return int(s / gcd(s, VL))


class PrefetchStrategy(object):
    def __init__(self, L1dist, L1dim, L2dist, L2dim):
        self.L1_dim = L1dim
        self.L1_dist = L1dist
        self.L2_dim = L2dim
        self.L2_dist = L2dist


def calculate_prefetch_and_unroll_strategy(
        r, mask_period, M, Tc, To, Cmin, Cmax, Vmin, Omax, use_gather, stride,
        fixed_dim, use_sizebased_omax):
    U = [1] * len(r)
    # Round U[0] up to nearest multiple of mask_period from Cmin
    # (If we use gather instructions, we must round up to nearest multiple of
    # stride instead since we need to fetch 8 elements each iteration.)
    min_unroll = stride if use_gather else mask_period
    U[0] = Cmin + min_unroll - 1
    U[0] = int(U[0] - U[0] % mask_period)
    U[0] = min(U[0], r[0])

    V_best = -1.0
    O_best = 1e40
    C_best = 0
    n_invalid_best = 1e40

    num_copies_inner = 1
    C_i = U[0] * num_copies_inner
    ## Ensure that we always run one iteration of the main loop; otherwise the
    ## prefetch distance and dimensions may not be set.
    Cmax = max(Cmax, C_i)
    unroll_dim = 0
    while C_i <= Cmax and unroll_dim < len(U):
        I_udim = num_unrolled_iterations(r[unroll_dim], U[unroll_dim])
        T_c_i = num_copies_inner * Tc
        T_o_i = To
        C1 = num_copies_inner
        C = U[unroll_dim] * C1
        for i in range(unroll_dim, len(U)):
            T_i = U[i] * T_c_i + T_o_i
            I_i = num_unrolled_iterations(r[i], U[i])
            D_i = lowest_timely_prefetch_dist(M, T_i)
            V_i = fraction_valid_prefetches(I_i, D_i)
            O_i = estimated_overshoot(D_i, T_i, M)
            if i + 1 >= len(r):
                n_invalid = 0
            else:
                n_invalid = (1 - V_i) * (r[i + 1] - 1)
            for j in range(i + 2, len(r)):
                n_invalid *= r[j]

            if use_sizebased_omax:
                L1_cachelines = 64 * 8
                max_lines = L1_cachelines / 4
                lines_per_iter = C
                max_iters_ahead = max_lines / lines_per_iter
                Omax_i = max_iters_ahead * Ti - M
            else:
                Omax_i = Omax

            if O_i < Omax_i or O_best > Omax_i and O_i < O_best:
                if fixed_dim is None or i == fixed_dim:
                    if n_invalid < n_invalid_best:
                        # print(
                        #     "U = {}, i = {}: C_i = {}, T_i = {}, I_i = {}, D_i = {}, V_i = {}, O_i = {}".
                        #     format(U, i, C_i, T_i, I_i, D_i, V_i, O_i))
                        best_U = [u for u in U]
                        best_L1 = (int(math.ceil(4.0 / C)), i)
                        best_L2 = (max(best_L1[0] + 1, D_i), i)
                        V_best = V_i
                        O_best = O_i
                        C_best = C_i
                        n_invalid_best = n_invalid
            C1 = r[i] * C1
            C = C1
            T_c_i = r[i] * T_c_i
            T_o_i = I_udim * To

        U[unroll_dim] = min(U[unroll_dim] + mask_period, r[unroll_dim])
        C_i = U[unroll_dim] * num_copies_inner
        if U[unroll_dim] == r[unroll_dim]:
            mask_period = 1
            unroll_dim += 1
            num_copies_inner = C_i

    return (PrefetchStrategy(best_L1[0], best_L1[1], best_L2[0], best_L2[1]),
            best_U)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-n", type=int, nargs="+", action="append", required=True)
    parser.add_argument(
        "-a", type=int, nargs="+", action="append", required=True)
    parser.add_argument(
        "-s", type=int, nargs="+", action="append", required=True)
    parser.add_argument(
        "-r", type=int, nargs="+", action="append", required=True)
    parser.add_argument("-sizes", type=int, nargs="+", required=True)
    parser.add_argument(
        "-element-sizes", type=int, choices=[4, 8], nargs="+", required=True)
    parser.add_argument("--repetitions", type=int, required=True)
    parser.add_argument("--runs", type=int, required=True)
    parser.add_argument("-o", "--org-outfile", dest="org_file", required=True)
    parser.add_argument("--debug-run", action="store_true")
    parser.add_argument(
        "-M",
        "--expected-memory-latency",
        type=int,
        default=[300],
        nargs="+",
        dest="M",
        help="Expected latency of a prefetch operation")

    parser.add_argument(
        "-Tc",
        "--cycles-per-vector-copy",
        type=int,
        default=[20],
        nargs="+",
        dest="Tc",
        help="Expected number of cycles per vector copy.")

    parser.add_argument(
        "-To",
        "--cycles-loop-overhead",
        type=int,
        default=[0],
        nargs="+",
        dest="To",
        help="Expected number of cycles of loop overhead.")

    parser.add_argument(
        "-Cmin",
        "--min-vector-copies",
        type=int,
        default=[1],
        nargs="+",
        dest="Cmin",
        help="The minimum number of vector copies in the unrolled loop body")

    parser.add_argument(
        "-Cmax",
        "--max-vector-copies",
        type=int,
        default=[7],
        nargs="+",
        dest="Cmax",
        help="The maximum number of vector copies in the unrolled loop body")

    parser.add_argument(
        "-Vmin",
        "--valid-prefetch-threshold",
        type=float,
        default=[0.95],
        nargs="+",
        dest="Vmin",
        help="Valid prefetch threshold")

    parser.add_argument(
        "-Omax",
        "--max-prefetch-overshoot",
        type=int,
        default=[1000],
        nargs="+",
        dest="Omax",
        help="The max number of cycles too early that prefetches should be")

    parser.add_argument("--use-gather", action="store_true", default=False)

    parser.add_argument(
        "--use-sizebased-omax", action="store_true", default=False)
    parser.add_argument(
        "--fixed-prefetch-dim",
        type=int,
        nargs="+",
        default=[None],
        dest="fixed_prefetch_dim")

    args, rest = parser.parse_known_args()

    if len(args.n) != len(args.r):
        print(
            "Incompatible lengths of arrays (n must equal r)", file=sys.stderr)
        sys.exit(1)

    if len(args.r) + 1 != len(args.s):
        print(
            "Incompatible lengths of arrays (r + 1 must equal s)",
            file=sys.stderr)
        sys.exit(1)

    if len(args.s) != len(args.a):
        print(
            "Incompatible lengths of arrays (s must equal a)", file=sys.stderr)
        sys.exit(1)

    if any([min(args.n[i]) < max(args.r[i]) for i in range(len(args.n))]):
        print(
            "All result dimensions must be <= source dimensions",
            file=sys.stderr)
        sys.exit(1)

    if any([min(args.n[i]) < max(args.a[i]) for i in range(len(args.n))]):
        print(
            "All start indices must be <= source dimensions", file=sys.stderr)
        sys.exit(1)

    if any([
            min(args.n[i]) < max(args.a[i]) +
        (max(args.r[i]) - 1) * max(args.s[i]) for i in range(len(args.n))
    ]):
        print("a_i + s_i*(r_i - 1) must be <= n_i", file=sys.stderr)
        sys.exit(1)

    num_dims = len(args.s)
    for point in itertools.product(
            *(args.n + args.a + args.r + args.s + [args.sizes] + [
                args.element_sizes
            ] + [args.fixed_prefetch_dim] + [args.M] + [args.Tc] + [args.To] +
              [args.Cmin] + [args.Cmax] + [args.Vmin] + [args.Omax])):
        n = point[0:num_dims - 1]
        a = point[num_dims - 1:2 * num_dims - 1]
        r = point[2 * num_dims - 1:3 * num_dims - 2]
        s = point[3 * num_dims - 2:4 * num_dims - 2]
        b = [a[i] + (r[i] - 1) * s[i] for i in range(num_dims - 1)]
        # print(len(args.n), len(args.r), len(args.s), len(args.a))
        # print(a)
        # print(b)
        # print(s)
        size = point[4 * num_dims - 2]
        elem_size = point[4 * num_dims - 1]
        fixed_dim = point[-8]
        M = point[-7]
        Tc = point[-6]
        To = point[-5]
        Cmin = point[-4]
        Cmax = point[-3]
        Vmin = point[-2]
        Omax = point[-1]
        final_n = compute_final_dim_size(a, b, s, n, size / elem_size)
        assert final_n > 0, "Cannot copy only {} bytes with the given inner dimension result sizes".format(
            size)
        n = n + (final_n, )
        b = b + [final_n]
        r = r + (final_n, )
        res_size = elem_size * final_n * functools.reduce(
            lambda x, y: x * y, r)
        src_size = elem_size * functools.reduce(lambda x, y: x * y, n)

        if args.debug_run:
            print(
                "({}) = {}, ({}) = {}, ({}) = {}, ({}) = {} \t=> {}, wanted {}, missed by {:6.2f} % (source size: {:7.2f} MiB)".
                format(",".join(["n" + str(i) for i in range(
                    1, num_dims + 1)]), n, ",".join(
                        ["a" + str(i) for i in range(1, num_dims + 1)]), a,
                       ",".join(["b" + str(i) for i in range(
                           1, num_dims + 1)]), b, ",".join([
                               "s" + str(i) for i in range(1, num_dims + 1)
                           ]), s, res_size, size, 100 * (res_size - size) /
                       size, src_size / (1024 * 1024)))
        else:
            ## Command example: ./run_fort_slice.py --threads 228 --runs 20 --repetitions 200 -n1 $n1 -n2 $n2 -n3 $n3 -a1 $a1 -b1 $b1 -s1 $s1 -a2 $a2 -b2 $b2 -s2 $s2 -a3 $a3 -b3 $b3 -s3 $s3 --real-width $width -o shitfaced-fort-experiment.org --org-append --overwrite-existing
            ## (the n1, n2 etc are \"dynamic\" up to n7, meaning that one may supply up to n7 but also fewer.)

            cmd = ["./run_slicec_cmp.py"]
            cmd.extend(["--runs", str(args.runs)])
            cmd.extend(["--repetitions", str(args.repetitions)])
            cmd.extend([
                arg
                for i in range(1, num_dims + 1)
                for arg in ["-n" + str(i), str(n[i - 1])]
            ])
            cmd.extend([
                arg
                for i in range(1, num_dims + 1)
                for arg in ["-a" + str(i), str(a[i - 1])]
            ])
            cmd.extend([
                arg
                for i in range(1, num_dims + 1)
                for arg in ["-b" + str(i), str(b[i - 1])]
            ])
            cmd.extend([
                arg
                for i in range(1, num_dims + 1)
                for arg in ["-s" + str(i), str(s[i - 1])]
            ])
            cmd.extend(["--real-width", str(elem_size)])
            cmd.extend(
                ["-o", args.org_file, "--org-append", "--overwrite-existing"])

            VL = 64 // elem_size
            mask_period = calculate_mask_period(s[0], VL)
            cache_lines_r = (int((b[0] - a[0]) // max(VL, s[0]) + 1), ) + r[1:]
            Omax = max(4, min(cache_lines_r[0], 128
                              if s[0] <= 2 else 192)) * Tc
            prefetch_strat, unroll_depths = calculate_prefetch_and_unroll_strategy(
                cache_lines_r, mask_period, M, Tc, To, Cmin, Cmax, Vmin, Omax,
                args.use_gather, s[0], fixed_dim, args.use_sizebased_omax)
            cmd.extend(["-M", str(M)])
            cmd.extend(["-Tc", str(Tc)])
            cmd.extend(["-To", str(To)])
            cmd.extend(["-Cmin", str(Cmin)])
            cmd.extend(["-Cmax", str(Cmax)])
            cmd.extend(["-Vmin", str(Vmin)])
            cmd.extend(["-Omax", str(Omax)])

            cmd.extend(["--L1-prefetch-dim", str(prefetch_strat.L1_dim)])
            cmd.extend(["--L1-prefetch-dist", str(prefetch_strat.L1_dist)])
            cmd.extend(["--L2-prefetch-dim", str(prefetch_strat.L2_dim)])
            cmd.extend(["--L2-prefetch-dist", str(prefetch_strat.L2_dist)])
            if args.use_gather:
                cmd.extend(["--use-gather"])
            cmd.extend(
                ["--unroll-config", "_".join([str(u) for u in unroll_depths])])

            cmd.extend(rest)

            exit_code = subprocess.call(cmd)
            if exit_code != 0:
                print(
                    ("Error in run: ({}) = {}, ({}) = {}, ({}) = {}, ({}) = {} "
                     + "(source size: {:7.2f} MiB, dest size {:7.2f} MiB)"
                     ).format(",".join(
                         ["n" + str(i)
                          for i in range(1, num_dims + 1)]), n, ",".join([
                              "a" + str(i) for i in range(1, num_dims + 1)
                          ]), a, ",".join(
                              ["b" + str(i)
                               for i in range(1, num_dims + 1)]), b, ",".join([
                                   "s" + str(i)
                                   for i in range(1, num_dims + 1)
                               ]), s, src_size / (1024 * 1024),
                              res_size / (1024 * 1024)),
                    file=sys.stderr)
                sys.exit(exit_code)


if __name__ == '__main__':
    main()
