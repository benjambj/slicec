.DEFAULT_GOAL = slicec

CXX = $(shell which icpc 2>/dev/null || which clang++)

bin/slicec_cmp_%: gensrc/slicec_cmp_%.f90 gensrc/do_slice_%.f90 obj/slicec_do_slice_%.o bin
	ifort -align all -pad -qopt-assume-safe-padding -fno-fnalias -fno-alias -openmp -mmic $(filter-out bin,$^) -O2 -o $@

.PRECIOUS: gensrc/slice_%.f90 gensrc/do_slice_%.f90 gensrc/slicec_do_slice_%.c gensrc/slicec_cmp_%.f90 configs/%_extended obj/slicec_do_slice_%.o

gensrc/slicec_cmp_%.f90: configs/%_extended slicec_cmp.f90.template
	cpp -imacros $< slicec_cmp.f90.template -o $@

gensrc/do_slice_%.f90: configs/%_extended do_slice.f90.template
	cpp -imacros $< do_slice.f90.template -o gensrc/do_slice_$*.f90

obj/slicec_do_slice_%.o: gensrc/slicec_do_slice_%.c obj
	icc -c -mmic -O2 -qopt-prefetch=0 -std=c99 $(filter-out obj,$^) -o $@

gensrc/slicec_do_slice_%.c: configs/%_extended slicec gensrc
	./slicec $(shell ./get_slicec_params.py $<) >$@

configs/%_extended: configs/% configs
	./gen_extra_config.py <$< >$@

slicec: slicec.cpp
	${CXX} -std=c++14 -O0 -g -o $@ $<

bin obj gensrc configs:
	mkdir -p $@

.PHONY: clean purge
clean:
	@-rm -r slicec obj

purge: clean
	@-rm -r gensrc bin configs
